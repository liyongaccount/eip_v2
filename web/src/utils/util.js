import enquireJs from 'enquire.js'
import { JSEncrypt } from 'jsencrypt'
export function isDef(v) {
    return v !== undefined && v !== null
}

/**
 * Remove an item from an array.
 */
export function remove(arr, item) {
    if (arr.length) {
        const index = arr.indexOf(item)
        if (index > -1) {
            return arr.splice(index, 1)
        }
    }
}

export function isRegExp(v) {
    return _toString.call(v) === '[object RegExp]'
}

export function enquireScreen(call) {
    const handler = {
        match: function() {
            call && call(true)
        },
        unmatch: function() {
            call && call(false)
        }
    }
    enquireJs.register('only screen and (max-width: 767.99px)', handler)
}

const _toString = Object.prototype.toString

export function newGuid() {
    var guid = "";
    for (var i = 1; i <= 32; i++) {
        var n = Math.floor(Math.random() * 16.0).toString(16);
        guid += n;
        if ((i == 8) || (i == 12) || (i == 16) || (i == 20))
            guid += "-";
    }
    return guid;
}

export function emptyGuid() {
    return "00000000-0000-0000-0000-000000000000";
}

export function selectTableRow(table, method, that, single = true) {
    var rows = table.getCheckboxRecords();
    if (rows.length == 0) {
        that.$message.warning("请选择需要操作的数据");
    } else if (single) {
        if (rows.length > 1) {
            that.$message.warning("您选择了多条数据,请取消多余勾选！");
        } else {
            method(rows[0]);
        }
    } else {
        method(rows);
    }
}
export function selectTableRowRadio(table, method, that) {
    var row = table.getRadioRecord();
    if (row) {
        method(row);
    } else {
        that.$message.warning("请选择需要操作的数据");
    }
}

export function deleteConfirm(msg, method, that) {
    that.$confirm({
        title: "删除提示?",
        content: msg,
        okText: "确定",
        okType: "danger",
        cancelText: "取消",
        onOk() {
            method()
        },
        onCancel() {},
    });
}
export function operationConfirm(msg, method, that) {
    that.$confirm({
        title: "操作提示?",
        content: msg,
        okText: "确定",
        okType: "danger",
        cancelText: "取消",
        onOk() {
            method()
        },
        onCancel() {},
    });
}
/**
 * 加密
 * @param {*} publicKey 
 * @param {*} data 
 * @returns 
 */
export function encryptedData(data) {
    let publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC9ds1UxO/ARslpqzBA4AIY7ejOJyYoe980wYSpMpfYb/cLyHl66c4uqm567CrigYxkt1Nv24h4L8Cy4v2aJm6T/Oi46a5DRHfbYFyHGwzan+iUqsNb37xPHkTd1KDuRYbvZsYZD15wUHPWczECXdj2FO0C4VDx6kriwJQv9PoqnwIDAQAB"
        // 新建JSEncrypt对象
    let encryptor = new JSEncrypt();
    // 设置公钥
    encryptor.setPublicKey(publicKey);
    // 加密数据
    return encryptor.encrypt(data);
}

/**
 * 解密
 * @param {*} privateKey 
 * @param {*} data 
 * @returns 
 */
export function decryptData(privateKey, data) {
    // 新建JSEncrypt对象
    let decrypt = new JSEncrypt();
    // 设置私钥
    decrypt.setPrivateKey(privateKey);
    // 解密数据
    return decrypt.decrypt(secretWord);
}