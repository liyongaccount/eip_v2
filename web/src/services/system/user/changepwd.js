import {
  SystemUserChangePassword
} from '@/services/api'
import {
  request,
  METHOD
} from '@/utils/request'

/**
 * 修改密码
 */
export function changepassword(param) {
  return request(SystemUserChangePassword, METHOD.POST, param)
}

export default {
  changepassword
}