import {
    SystemTypeQuery,
    SystemTypeDelete,
    SystemTypeIsFreeze
} from '@/services/api'
import { request, METHOD } from '@/utils/request'
/**
 * 列表
 */
export function query(param) {
    return request(SystemTypeQuery, METHOD.POST, param)
}

/**
 * 删除
 */
export async function del(param) {
    return request(SystemTypeDelete, METHOD.POST, param)
}
/**
 * 根据Id获取
 */
export function findById(param) {
    return request(SystemTypeFindById, METHOD.GET, param)
}

/**
 * 
 */
export function isFreeze(param) {
    return request(SystemTypeIsFreeze, METHOD.POST, param)
}
export default {
    query,
    del,
    isFreeze
}