import {
    SystemMenuButtonSave,
    AgileConfigSave,
    AgileConfigSaveType,
    AgileDataBaseIsTableExist,
    AgileConfigPublicJson,
    AgileDataBaseSaveFormTable,
    AgileConfigFindByMenuId,
    SystemTypeAll,
    SystemMenuSave,
    SystemMenuFindById,
} from '@/services/api'
import { request, METHOD } from '@/utils/request'
/**
 * 保存
 */
export async function buttonSave(form) {
    return request(SystemMenuButtonSave, METHOD.POST, form)
}
/**
 * 保存
 */
export async function agileSave(form) {
    return request(AgileConfigSave, METHOD.POST, form)
}
/**
 * 保存
 */
export async function agileSaveType(form) {
    return request(AgileConfigSaveType, METHOD.POST, form)
}

/**
 * 表是否存在
 */
export function tableExist(param) {
    return request(AgileDataBaseIsTableExist, METHOD.POST, param)
}

/**
 * 创建表
 */
export function table(param) {
    return request(AgileDataBaseSaveFormTable, METHOD.POST, param)
}

/**
 * 发布
 */
export async function listPublic(form) {
    return request(AgileConfigPublicJson, METHOD.POST, form)
}
/**
 * 根据Id获取
 */
export function findById(id) {
    return request(SystemMenuFindById + "/" + id, METHOD.GET, {})
}

/**
 * 保存
 */
export async function save(param) {
    return request(SystemMenuSave, METHOD.POST, param)
}

/**
 * 根据Id获取
 */
export function findByMenuId(param) {
    return request(AgileConfigFindByMenuId, METHOD.POST, param)
}

/**
 * 敏捷开发类型
 */
export function systemType(param) {
    return request(SystemTypeAll, METHOD.POST, param)
}
export default {
    save,
    findById,
    agileSave,
    agileSaveType,
    tableExist,
    findById,
    listPublic,
    buttonSave,
    findByMenuId,
    systemType
}