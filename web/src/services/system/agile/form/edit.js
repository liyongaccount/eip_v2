import { SystemTypeAll, AgileConfigSave, AgileConfigFindById, AgileDataBaseIsTableExist, AgileDataBaseSaveFormTable } from '@/services/api'
import { request, METHOD } from '@/utils/request'

/**
 * 保存
 */
export async function save(form) {
    return request(AgileConfigSave, METHOD.POST, form)
}

/**
 * 根据Id获取
 */
export function findById(id) {
    return request(AgileConfigFindById + "/" + id, METHOD.GET, {})
}

/**
 * 表是否存在
 */
export function tableExist(param) {
    return request(AgileDataBaseIsTableExist, METHOD.POST, param)
}

/**
 * 创建表
 */
export function table(param) {
    return request(AgileDataBaseSaveFormTable, METHOD.POST, param)
}
/**
 * 敏捷开发类型
 */
export function systemType(param) {
    return request(SystemTypeAll, METHOD.POST, param)
}
export default {
    save,
    findById,
    tableExist,
    table,
    systemType
}