import {
    AgileEventApi,
    AgileConfigQuery,
    AgileDataBaseBusinessDataQuery,
    AgileDataBaseBusinessDataDel,

    WorkflowEngineRevokeByCreateUser
} from '@/services/api'
import {
    request,
    requestSync,
    METHOD
} from '@/utils/request'

/**
 * 查询业务数据
 */
export async function queryConfig(param) {
    return request(AgileConfigQuery, METHOD.POST, param)
}

/**
 * 查询业务数据
 */
export async function query(param) {
    return request(AgileDataBaseBusinessDataQuery, METHOD.POST, param)
}

/**
 * 删除业务数据
 */
export async function del(param) {
    return request(AgileDataBaseBusinessDataDel, METHOD.POST, param)
}
/**
 * 调用api
 */
export function eventApi(param) {
    return requestSync(AgileEventApi, METHOD.POST, param)
}

/**
 * 
 */
export function workflowEngineRevokeByCreateUser(param) {
    return request(WorkflowEngineRevokeByCreateUser, METHOD.POST, param)
}
export default {
    query,
    queryConfig,
    del,
    eventApi,
    workflowEngineRevokeByCreateUser
}