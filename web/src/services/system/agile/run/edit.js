import { SystemSnQuery, SystemFileCorrelationId, SystemFileUploadDel, AgileEventApi, AgileDataBaseBusinessDataFindFormSource, AgileDataBaseBusinessData, AgileDataBaseBusinessDataFindById, AgileDataBaseBusinessDataQueryBatch } from '@/services/api'
import { request, requestSync, METHOD } from '@/utils/request'

/**
 * 保存业务数据
 */
export function businessData(param) {
    return request(AgileDataBaseBusinessData, METHOD.POST, param)
}
/**
 * 根据Id获取业务数据
 */
export function businessDataById(param) {
    return request(AgileDataBaseBusinessDataFindById, METHOD.POST, param)
}

/**
 * 保存业务数据
 */
export function businessDataBatch(param) {
    return request(AgileDataBaseBusinessDataQueryBatch, METHOD.POST, param)
}


/**
 * 保存业务数据
 */
export async function businessDataFormSource(param) {
    return await requestSync(AgileDataBaseBusinessDataFindFormSource, METHOD.POST, param)
}


/**
 * 调用api
 */
export function eventApi(param) {
    return requestSync(AgileEventApi, METHOD.POST, param)
}

/**
 * 删除附件
 */
export function fileUploadDel(param) {
    return requestSync(SystemFileUploadDel, METHOD.POST, param)
}

/**
 * 获取附件
 */
export function fileCorrelationId(id) {
    return requestSync(SystemFileCorrelationId + "/" + id, METHOD.GET, {})
}
/**
 * 获取编码
 */
export function snQuery(param) {
    return requestSync(SystemSnQuery, METHOD.POST, param)
}
export default {
    eventApi,
    businessData,
    businessDataById,
    businessDataBatch,
    businessDataFormSource,
    fileUploadDel,
    fileCorrelationId,
    snQuery
}