import {
    AgileDataBaseTable,
    AgileDataBaseProc,
    AgileDataBaseView,
    AgileDataBaseTableColumn
} from '@/services/api'
import { request, METHOD } from '@/utils/request'

/**
 * 表
 */
export async function table() {
    return request(AgileDataBaseTable, METHOD.GET, {})
}

/**
 * 视图
 */
export async function view() {
    return request(AgileDataBaseView, METHOD.GET, {})
}

/**
 * 存储过程
 */
export async function proc() {
    return request(AgileDataBaseProc, METHOD.GET, {})
}

/**
 * 列
 */
export async function column(param) {
    return request(AgileDataBaseTableColumn, METHOD.POST, param)
}

export default {
    table,
    view,
    proc,
    column
}