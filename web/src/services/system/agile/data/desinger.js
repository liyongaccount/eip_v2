import { AgileConfigSaveJson, AgileConfigPublicJson, AgileConfigSaveJson, AgileConfigPublicJson, AgileConfigFindById, AgileDataBaseSaveFormTableField } from '@/services/api'
import { request, METHOD } from '@/utils/request'

/**
 * 保存
 */
export async function editSave(form) {
    return request(AgileConfigSaveJson, METHOD.POST, form)
}
/**
 * 发布
 */
export async function editPublic(form) {
    return request(AgileConfigPublicJson, METHOD.POST, form)
}

/**
 * 保存
 */
export async function listSave(form) {
    return request(AgileConfigSaveJson, METHOD.POST, form)
}
/**
 * 发布
 */
export async function listPublic(form) {
    return request(AgileConfigPublicJson, METHOD.POST, form)
}

/**
 * 修改表字段
 */
export async function tableField(param) {
    return request(AgileDataBaseSaveFormTableField, METHOD.POST, param)
}
/**
 * 根据Id获取
 */
export async function findById(id) {
    return request(AgileConfigFindById + "/" + id, METHOD.GET, {})
}
export default {
    editSave,
    editPublic,
    listSave,
    listPublic,
    findById
}