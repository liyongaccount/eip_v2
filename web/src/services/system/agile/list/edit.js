import {
    AgileConfigQuery,
    AgileConfigSave,
    AgileConfigFindById,
    AgileDataBaseTable,
    AgileDataBaseView,
    AgileDataBaseProc
} from '@/services/api'
import {
    request,
    METHOD
} from '@/utils/request'

/**
 * 保存
 */
export async function save(form) {
    return request(AgileConfigSave, METHOD.POST, form)
}

/**
 * 根据Id获取
 */
export function findById(id) {
    return request(AgileConfigFindById + "/" + id, METHOD.GET, {})
}
/**
 * 表
 */
export async function table() {
    return request(AgileDataBaseTable, METHOD.GET, {})
}

/**
 * 视图
 */
export async function view() {
    return request(AgileDataBaseView, METHOD.GET, {})
}

/**
 * 存储过程
 */
export async function proc() {
    return request(AgileDataBaseProc, METHOD.GET, {})
}

/**
 * 列表
 */
export async function query(param) {
    return request(AgileConfigQuery, METHOD.POST, param)
}
export default {
    save,
    findById,
    table,
    view,
    proc,
    query
}