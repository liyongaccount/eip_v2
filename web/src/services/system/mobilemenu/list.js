import {
    SystemMobileMenu,
    SystemMobileMenuQuery,
    SystemMobileMenuDelete,
    SystemMobileMenuHaveMenuPermission,
    SystemMobileMenuHaveDataPermission,
    SystemMobileMenuHaveFieldPermission,
    SystemMobileMenuHaveButtonPermission,
    SystemMobileMenuIsFreeze
} from '@/services/api'
import { request, METHOD } from '@/utils/request'

/**
 * 树
 */
export function menuQuery() {
    return request(SystemMobileMenu, METHOD.GET, {})
}

/**
 * 列表
 */
export function query(param) {
    return request(SystemMobileMenuQuery, METHOD.POST, param)
}

/**
 * 删除
 */
export async function del(param) {
    return request(SystemMobileMenuDelete, METHOD.POST, param)
}
/**
 * 
 */
export function haveMenuPermission(param) {
    return request(SystemMobileMenuHaveMenuPermission, METHOD.POST, param)
}
/**
 * 
 */
export function haveDataPermission(param) {
    return request(SystemMobileMenuHaveDataPermission, METHOD.POST, param)
}
/**
 * 
 */
export function haveFieldPermission(param) {
    return request(SystemMobileMenuHaveFieldPermission, METHOD.POST, param)
}
/**
 * 
 */
export function haveButtonPermission(param) {
    return request(SystemMobileMenuHaveButtonPermission, METHOD.POST, param)
}
/**
 * 
 */
export function isFreeze(param) {
    return request(SystemMobileMenuIsFreeze, METHOD.POST, param)
}
export default {
    menuQuery,
    query,
    del,
    haveMenuPermission,
    haveDataPermission,
    haveFieldPermission,
    haveButtonPermission,
    isFreeze
}