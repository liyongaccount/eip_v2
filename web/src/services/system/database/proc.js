import { AgileDataBaseProc } from '@/services/api'
import { request, METHOD } from '@/utils/request'

/**
 * 空间
 */
export function proc() {
    return request(AgileDataBaseProc, METHOD.GET, {})
}

export default {
    proc
}