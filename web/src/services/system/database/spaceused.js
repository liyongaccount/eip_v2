import { AgileDataBaseTableSpaceused } from '@/services/api'
import { request, METHOD } from '@/utils/request'

/**
 * 空间
 */
export function spaceused() {
    return request(AgileDataBaseTableSpaceused, METHOD.GET, {})
}

export default {
    spaceused
}