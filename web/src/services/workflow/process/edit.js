import {
    WorkflowProcessSave,
    WorkflowProcessFindById,
    AgileConfigBase,
    SystemTypeAll
} from '@/services/api'
import {
    request,
    METHOD
} from '@/utils/request'
/**
 * 基础表单
 */
export function findForm(param) {
    return request(AgileConfigBase, METHOD.POST, param)
}
/**
 * 保存
 */
export async function save(form) {
    return request(WorkflowProcessSave, METHOD.POST, form)
}

/**
 * 根据Id获取
 */
export function findById(id) {
    return request(WorkflowProcessFindById + "/" + id, METHOD.GET, {})
}
/**
 * 类型
 */
export function systemType(param) {
    return request(SystemTypeAll, METHOD.POST, param)
}

export default {
    findForm,
    save,
    findById,
    systemType
}