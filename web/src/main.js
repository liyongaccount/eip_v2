import Vue from 'vue'
import App from './App.vue'
import { initRouter } from './router'
import './theme/index.less'
import store from './store'
import 'animate.css/source/animate.css'
import Plugins from '@/plugins'
import { initI18n } from '@/utils/i18n'
import bootstrap from '@/bootstrap'
import 'moment/locale/zh-cn'
import './utils/lazy_use'
import './utils/lazy_use_table'
import 'vxe-table/lib/style.css'
import '@/utils/prototype'

import vcolorpicker from 'vcolorpicker'
Vue.use(vcolorpicker)

const router = initRouter(store.state.setting.asyncRoutes)
const i18n = initI18n('CN', 'US')

Vue.config.productionTip = false
Vue.use(Plugins)

bootstrap({ router, store, i18n, message: Vue.prototype.$message })
import './utils/plugins.js'

// 引入配置
import './mobile'

import loading from '@/components/loading/loading'; // 引入loading
Vue.use(loading); // 全局使用loading

new Vue({
    router,
    store,
    i18n,
    render: h => h(App),
}).$mount('#app')