/*
 * author kcz
 * date 2019-11-20
 * description 表单控件项
 */
// 基础控件
export const basicsList = [{
        type: "input", // 表单类型
        label: "输入框", // 标题文字
        icon: "icon-write",
        options: {
            span: 24,
            type: "text",
            width: "100%", // 宽度
            defaultValue: undefined, // 默认值
            defaultValueType: undefined, //类型
            defaultValueBillNo: undefined, //规则编号
            defaultValueApiPath: undefined, //接口地址
            placeholder: "请输入", // 没有输入时，提示文字
            clearable: false,
            maxLength: 128,
            addonBefore: "",
            addonAfter: "",
            showLabel: true,
            hidden: false, // 是否隐藏，false显示，true隐藏
            disabled: false // 是否禁用，false不禁用，true禁用
        },
        model: "", // 数据字段
        key: "",
        help: "",
        rules: [
            //验证规则
            {
                required: false, // 必须填写
                message: "必填项",

            }
        ],
        event: {
            change: "" //改变事件
        }
    },
    {
        type: "textarea", // 表单类型
        label: "文本框", // 标题文字
        icon: "icon-edit",
        options: {
            span: 24,
            width: "100%", // 宽度
            minRows: 4,
            maxRows: 6,
            maxLength: 128,
            defaultValue: "",
            clearable: false,
            showLabel: true,
            hidden: false, // 是否隐藏，false显示，true隐藏
            disabled: false,
            placeholder: "请输入"
        },
        model: "", // 数据字段
        key: "",
        help: "",
        rules: [{
            required: false,
            message: "必填项",

        }],
        event: {
            change: "" //改变事件
        }
    },
    {
        type: "number", // 表单类型
        label: "数字输入框", // 标题文字
        icon: "icon-number",
        options: {
            span: 24,
            width: "100%", // 宽度
            defaultValue: 0, // 默认值
            min: null, // 可输入最小值
            max: null, // 可输入最大值
            precision: null,
            showLabel: true,
            step: 1, // 步长，点击加减按钮时候，加减多少
            hidden: false, // 是否隐藏，false显示，true隐藏
            disabled: false, //是否禁用
            placeholder: "请输入"
        },
        model: "", // 数据字段
        key: "",
        help: "",
        rules: [{
            required: false,
            message: "必填项"
        }],
        event: {
            change: "" //改变事件
        }
    },
    {
        type: "select", // 表单类型
        label: "下拉选择器", // 标题文字
        icon: "icon-xiala",
        options: {
            span: 24,
            width: "100%", // 宽度
            defaultValue: undefined, // 下拉选框请使用undefined为默认值
            multiple: false, // 是否允许多选
            disabled: false, // 是否禁用
            clearable: false, // 是否显示清除按钮
            hidden: false, // 是否隐藏，false显示，true隐藏
            placeholder: "请选择", // 默认提示文字
            showLabel: true,
            dynamic: false,
            dynamicConfig: {
                source: {
                    type: 'table', //数据来源:table,view,proc
                    name: undefined, //对应数据源名称:表名,视图名称,存储过程名称
                    condition: [], //条件
                    api: {
                        type: 'post', //请求类型post,get
                        url: '', //请求路径
                    },
                },
                title: '',
                key: undefined, //键
                value: undefined, //值
                sidx: undefined, //排序字段
                sord: 'desc', //排序方式desc,asc
                map: [], //数据映射,选择后会将映射数据值对应显示上
            },
            options: [
                // 下拉选择项配置
                {
                    value: "1",
                    label: "下拉框1"
                },
                {
                    value: "2",
                    label: "下拉框2"
                }
            ],
            showSearch: false // 是否显示搜索框，搜索选择的项的值，而不是文字
        },
        model: "",
        key: "",
        help: "",
        rules: [{
            required: false,
            message: "必填项",

        }],
        event: {
            change: "" //改变事件
        }
    },
    {
        type: "checkbox",
        label: "多选框",
        icon: "icon-duoxuan1",
        options: {
            span: 24,
            disabled: false, //是否禁用
            hidden: false, // 是否隐藏，false显示，true隐藏
            defaultValue: [],
            dynamic: false,
            showLabel: true,
            dynamicConfig: {
                source: {
                    type: 'table', //数据来源:table,view,proc
                    name: undefined, //对应数据源名称:表名,视图名称,存储过程名称
                    condition: [], //条件
                    api: {
                        type: 'post', //请求类型post,get
                        url: '', //请求路径
                    },
                },
                title: '',
                key: undefined, //键
                value: undefined, //值
                sidx: undefined, //排序字段
                sord: 'desc', //排序方式desc,asc
                map: [], //数据映射,选择后会将映射数据值对应显示上
            },
            options: [{
                    value: "1",
                    label: "选项1"
                },
                {
                    value: "2",
                    label: "选项2"
                },
                {
                    value: "3",
                    label: "选项3"
                }
            ]
        },
        model: "",
        key: "",
        help: "",
        rules: [{
            required: false,
            message: "必填项",

        }],
        event: {
            change: "" //改变事件
        }
    },
    {
        type: "radio", // 表单类型
        label: "单选框", // 标题文字
        icon: "icon-danxuan-cuxiantiao",
        options: {
            span: 24,
            disabled: false, //是否禁用
            hidden: false, // 是否隐藏，false显示，true隐藏
            defaultValue: "", // 默认值
            dynamic: false,
            showLabel: true,
            dynamicConfig: {
                source: {
                    type: 'table', //数据来源:table,view,proc
                    name: undefined, //对应数据源名称:表名,视图名称,存储过程名称
                    condition: [], //条件
                    api: {
                        type: 'post', //请求类型post,get
                        url: '', //请求路径
                    },
                },
                title: '',
                key: undefined, //键
                value: undefined, //值
                sidx: undefined, //排序字段
                sord: 'desc', //排序方式desc,asc
                map: [], //数据映射,选择后会将映射数据值对应显示上
            },
            options: [{
                    value: "1",
                    label: "选项1"
                },
                {
                    value: "2",
                    label: "选项2"
                },
                {
                    value: "3",
                    label: "选项3"
                }
            ]
        },
        model: "",
        key: "",
        help: "",
        rules: [{
            required: false,
            message: "必填项",

        }],
        event: {
            change: "" //改变事件
        }
    },
    {
        type: "date", // 表单类型
        label: "日期选择框", // 标题文字
        icon: "icon-calendar",
        options: {
            span: 24,
            width: "100%", // 宽度
            defaultValue: "", // 默认值，字符串 12:00:00
            rangeDefaultValue: [], // 默认值，字符串 12:00:00
            range: false, // 范围日期选择，为true则会显示两个时间选择框（同时defaultValue和placeholder要改成数组），
            showTime: false, // 是否显示时间选择器
            showTimeNow: false, // 是否显示当前时间
            disabled: false, // 是否禁用
            hidden: false, // 是否隐藏，false显示，true隐藏
            clearable: false, // 是否显示清除按钮
            showLabel: true,
            placeholder: "请选择",
            rangePlaceholder: ["开始时间", "结束时间"],
            format: "YYYY-MM-DD" // 展示格式  （请按照这个规则写 YYYY-MM-DD HH:mm:ss，区分大小写）
        },
        model: "",
        key: "",
        help: "",
        rules: [{
            required: false,
            message: "必填项",

        }],
        event: {
            change: "" //改变事件
        }
    },
    {
        type: "time", // 表单类型
        label: "时间选择框", // 标题文字
        icon: "icon-time",
        options: {
            span: 24,
            width: "100%", // 宽度
            defaultValue: "", // 默认值，字符串 12:00:00
            disabled: false, // 是否禁用
            hidden: false, // 是否隐藏，false显示，true隐藏
            clearable: false, // 是否显示清除按钮
            showLabel: true,
            placeholder: "请选择",
            format: "HH:mm:ss" // 展示格式
        },
        model: "",
        key: "",
        help: "",
        rules: [{
            required: false,
            message: "必填项",

        }],
        event: {
            change: "" //改变事件
        }
    },
    {
        type: "rate", // 表单类型
        label: "评分", // 标题文字
        icon: "icon-pingfen_moren",
        options: {
            span: 24,
            defaultValue: 0,
            max: 5, // 最大值
            showLabel: true,
            disabled: false, // 是否禁用
            hidden: false, // 是否隐藏，false显示，true隐藏
            allowHalf: false // 是否允许半选
        },
        model: "",
        key: "",
        help: "",
        rules: [{
            required: false,
            message: "必填项",

        }],
        event: {
            change: "" //改变事件
        }
    },
    {
        type: "slider", // 表单类型
        label: "滑动输入条", // 标题文字
        icon: "icon-menu",
        options: {
            span: 24,
            width: "100%", // 宽度
            defaultValue: 0, // 默认值， 如果range为true的时候，则需要改成数组,如：[12,15]
            disabled: false, // 是否禁用
            hidden: false, // 是否隐藏，false显示，true隐藏
            min: 0, // 最小值
            max: 100, // 最大值
            step: 1, // 步长，取值必须大于 0，并且可被 (max - min) 整除
            showLabel: true,
            showInput: false // 是否显示输入框，range为true时，请勿开启
                // range: false // 双滑块模式
        },
        model: "",
        key: "",
        help: "",
        rules: [{
            required: false,
            message: "必填项",

        }],
        event: {
            change: "" //改变事件
        }
    },
    {
        type: "uploadFile", // 表单类型
        label: "上传文件", // 标题文字
        icon: "icon-upload",
        options: {
            span: 24,
            defaultValue: "[]",
            multiple: false,
            disabled: false,
            hidden: false, // 是否隐藏，false显示，true隐藏
            drag: false,
            pdfShow: false,
            showLabel: true,
            downloadWay: "a",
            dynamicFun: "",
            width: "100%",
            limit: 3,
            data: "{}",
            fileName: "file",
            headers: {},
            action: "/system/file/upload",
            placeholder: "上传",
            accept: ""
        },
        model: "",
        key: "",
        help: "",
        rules: [{
            required: false,
            message: "必填项"
        }],
        event: {
            change: "" //改变事件
        }
    },
    {
        type: "uploadImg",
        label: "上传图片",
        icon: "icon-image",
        options: {
            span: 24,
            defaultValue: "[]",
            multiple: false,
            hidden: false, // 是否隐藏，false显示，true隐藏
            disabled: false,
            showLabel: true,
            width: "100%",
            data: "{}",
            limit: 3,
            placeholder: "上传",
            fileName: "image",
            headers: {},
            action: "/system/file/upload",
            listType: "picture-card"
        },
        model: "",
        key: "",
        help: "",
        rules: [{
            required: false,
            message: "必填项"
        }],
        event: {
            change: "" //改变事件
        }
    },
    {
        type: "treeSelect", // 表单类型
        label: "树选择器", // 标题文字
        icon: "icon-tree",
        options: {
            span: 24,
            disabled: false, //是否禁用
            defaultValue: undefined, // 默认值
            multiple: false,
            hidden: false, // 是否隐藏，false显示，true隐藏
            clearable: false, // 是否显示清除按钮
            showSearch: false, // 是否显示搜索框，搜索选择的项的值，而不是文字
            treeCheckable: false,
            showLabel: true,
            placeholder: "请选择",
            dynamic: true,
            options: [{
                    value: "1",
                    label: "选项1",
                    children: [{
                        value: "11",
                        label: "选项11"
                    }]
                },
                {
                    value: "2",
                    label: "选项2",
                    children: [{
                        value: "22",
                        label: "选项22"
                    }]
                }
            ]
        },
        model: "",
        key: "",
        help: "",
        rules: [{
            required: false,
            message: "必填项",

        }],
        event: {
            change: "" //改变事件
        }
    },
    {
        type: "cascader", // 表单类型
        label: "级联选择器", // 标题文字
        icon: "icon-guanlian",
        options: {
            span: 24,
            disabled: false, //是否禁用
            hidden: false, // 是否隐藏，false显示，true隐藏
            defaultValue: undefined, // 默认值
            showSearch: false, // 是否显示搜索框，搜索选择的项的值，而不是文字
            placeholder: "请选择",
            clearable: false, // 是否显示清除按钮
            showLabel: true,
            dynamic: true,
            options: [{
                    value: "1",
                    label: "选项1",
                    children: [{
                        value: "11",
                        label: "选项11"
                    }]
                },
                {
                    value: "2",
                    label: "选项2",
                    children: [{
                        value: "22",
                        label: "选项22"
                    }]
                }
            ]
        },
        model: "",
        key: "",
        help: "",
        rules: [{
            required: false,
            message: "必填项",

        }],
        event: {
            change: "" //改变事件
        }
    },
    {
        type: "batch",
        label: "动态表格",
        icon: "icon-biaoge",
        list: [],
        options: {
            span: 24,
            scrollY: 300,
            disabled: false,
            hidden: false, // 是否隐藏，false显示，true隐藏
            showLabel: false,
            hideSequence: false,
            width: "100%"
        },
        model: "",
        key: "",
        help: ""
    },
    {
        type: "selectInputList",
        label: "选择输入列",
        icon: "icon-biaoge",
        columns: [{
                value: "1",
                label: "选项1",
                list: []
            },
            {
                value: "2",
                label: "选项2",
                list: []
            }
        ],
        options: {
            span: 24,
            disabled: false,
            multiple: true, // 是否允许多选
            hidden: false, // 是否隐藏，false显示，true隐藏
            showLabel: false,
            width: "100%"
        },
        model: "",
        key: "",
        help: "",
        event: {
            change: "" //改变事件
        }
    },
    {
        type: "editor",
        label: "富文本",
        icon: "icon-LC_icon_edit_line_1",
        list: [],
        options: {
            span: 24,
            height: 300,
            placeholder: "请输入",
            defaultValue: "",
            chinesization: true,
            hidden: false, // 是否隐藏，false显示，true隐藏
            disabled: false,
            showLabel: false,
            width: "100%"
        },
        model: "",
        key: "",
        help: "",
        rules: [{
            required: false,
            message: "必填项",

        }],
        event: {
            change: "" //改变事件
        }
    },
    {
        type: "switch", // 表单类型
        label: "开关", // 标题文字
        icon: "icon-kaiguan3",
        options: {
            showLabel: true,
            span: 24,
            defaultValue: false, // 默认值 Boolean 类型
            hidden: false, // 是否隐藏，false显示，true隐藏
            disabled: false // 是否禁用
        },
        model: "",
        key: "",
        help: "",
        rules: [{
            required: false,
            message: "必填项"
        }],
        event: {
            change: "" //改变事件
        }
    },
    {
        type: "button", // 表单类型
        label: "按钮", // 标题文字
        icon: "icon-button-remove",
        options: {
            type: "primary",
            handle: "submit",
            dynamicFun: "",
            hidden: false, // 是否隐藏，false显示，true隐藏
            disabled: false, // 是否禁用，false不禁用，true禁用
            span: 24,
            size: "default",
        },
        key: ""
    },
    {
        type: "alert",
        label: "警告提示",
        icon: "icon-zu",
        options: {
            span: 24,
            type: "success",
            description: "",
            showIcon: false,
            banner: false,
            hidden: false, // 是否隐藏，false显示，true隐藏
            closable: false
        },
        key: ""
    },
    {
        type: "text",
        label: "文字",
        icon: "icon-zihao",
        options: {
            span: 24,
            textAlign: "right",
            hidden: false, // 是否隐藏，false显示，true隐藏
            showRequiredMark: false,
            color: "rgb(0, 0, 0)",
            fontFamily: "SimHei",
            color: "rgba(0, 0, 0, 0.9)",
            fontSize: "10.5pt"
        },
        key: ""
    },
    {
        type: "html",
        label: "HTML",
        icon: "icon-ai-code",
        options: {
            span: 24,
            hidden: false, // 是否隐藏，false显示，true隐藏
            defaultValue: "<strong>HTML</strong>"
        },
        key: ""
    }
];

// 高级控件
// export const highList = [];

// import { Alert } from "ant-design-vue";

// 自定义组件
export const customComponents = {
    title: "自定义组件",
    list: [
        // {
        //   label: "测试",
        //   type: "jkjksdf",
        //   component: Alert,
        //   options: {
        //     multiple: false,
        //     disabled: false,
        //     width: "100%",
        //     data: "{}",
        //     limit: 3,
        //     placeholder: "上传",
        //     action: "",
        //     listType: "picture-card"
        //   },
        //   model: "",
        //   key: "",
        //   rules: [
        //     {
        //       required: false,
        //       message: "必填项"
        //     }
        //   ]
        // }
    ]
};
// window.$customComponentList = customComponents.list;

// 布局控件
export const layoutList = [{
        type: "divider",
        label: "分割线",
        icon: "icon-fengexian",
        options: {
            orientation: "left"
        },
        key: "",
        model: ""
    },
    {
        type: "card",
        label: "卡片布局",
        icon: "icon-qiapian",
        list: [],
        options: {
            span: 24,
            size: "default",
        },
        key: "",
        model: ""
    },
    {
        type: "tabs",
        label: "标签页布局",
        icon: "icon-tabs",
        options: {
            span: 24,
            tabBarGutter: null,
            type: "line",
            tabPosition: "top",
            size: "default",
            animated: true
        },
        columns: [{
                value: "1",
                label: "选项1",
                list: []
            },
            {
                value: "2",
                label: "选项2",
                list: []
            }
        ],
        key: "",
        model: ""
    },
    {
        type: "grid",
        label: "栅格布局",
        icon: "icon-zhage",
        columns: [{
                span: 12,
                list: []
            },
            {
                span: 12,
                list: []
            }
        ],
        options: {
            span: 24,
            gutter: 0
        },
        key: "",
        model: ""
    },
    {
        type: "table",
        label: "表格布局",
        icon: "icon-biaoge",
        trs: [{
                tds: [{
                        colspan: 1,
                        rowspan: 1,
                        style: 'background: #f1f9fe;border: 1px solid #b7d4ec',
                        list: []
                    },
                    {
                        colspan: 1,
                        rowspan: 1,
                        style: 'border: 1px solid #b7d4ec',
                        list: []
                    }
                ]
            },
            {
                tds: [{
                        colspan: 1,
                        rowspan: 1,
                        style: 'background: #f1f9fe;border: 1px solid #b7d4ec',
                        list: []
                    },
                    {
                        colspan: 1,
                        rowspan: 1,
                        style: 'border: 1px solid #b7d4ec',
                        list: []
                    }
                ]
            }
        ],
        options: {
            span: 24,
            width: 850,
            unit: 'px',
            formWidth: 800,
            formWidthUnit: 'px',
            top: "100", //距离顶部高度,默认100
            centered: false, //垂直居中
            bordered: true,
            bright: false,
            small: true,
            customStyle: ""
        },
        key: "",
        model: ""
    }
];