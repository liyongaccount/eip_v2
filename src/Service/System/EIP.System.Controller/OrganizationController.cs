﻿/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/

using EIP.Base.Models.Entities.System;
using EIP.Base.Models.Resx;
using EIP.Common.Controller.Attribute;
using EIP.Common.Extension;
using EIP.Common.Models;
using EIP.Common.Models.Dtos;
using EIP.System.Logic;
using EIP.System.Models.Dtos.Organization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
namespace EIP.System.Controller
{
    /// <summary>
    /// 组织机构
    /// </summary>

    public class OrganizationController : BaseSystemController
    {
        #region 构造函数
        private readonly ISystemOrganizationLogic _organizationLogic;
        private readonly ISystemPermissionLogic _permissionLogic;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="organizationLogic"></param>
        /// <param name="permissionLogic"></param>
        public OrganizationController(ISystemOrganizationLogic organizationLogic,
            ISystemPermissionLogic permissionLogic)
        {
            _organizationLogic = organizationLogic;
            _permissionLogic = permissionLogic;
        }

        #endregion

        #region 方法

        /// <summary>
        /// 读取组织机构树上下级关系
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Remark("组织机构维护-方法-列表-读取组织机构树上下级关系", RemarkFrom.System)]
        [Route("/system/organization/range/{range}")]
        public async Task<JsonResult> FindOrganizationRange([FromRoute] int range = 0)
        {
            #region 获取权限控制器信息
            string sql = " 1=1 ";
            if (!CurrentUser.IsAdmin)
            {
                switch (range)
                {
                    //当前部门及下级部门
                    case 1:
                        sql = $" org.ParentIds like '%{CurrentUser.OrganizationId}%'";
                        break;
                    //下级部门
                    case 2:
                        sql = $" org.ParentIds like '%{CurrentUser.OrganizationId},%'";
                        break;
                    //当前公司
                    case 3:
                    case 4:
                    case 5:
                        sql = $" org.Nature=1 ";
                        break;
                }
            }
            SystemOrganizationDataPermissionInput input = new SystemOrganizationDataPermissionInput
            {
                PrincipalUser = CurrentUser,
                DataSql = sql,
                Range = range
            };
            #endregion
            return JsonForTree((await _organizationLogic.FindDataPermissionTree(input)).Data);
        }


        /// <summary>
        /// 读取树结构:排除下级
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Remark("用户控件-方法-读取组织机构树:排除下级", RemarkFrom.System)]
        [Route("/system/organization/toporg/{topOrg}")]
        public async Task<JsonResult> FindTreeAll([FromRoute] string topOrg)
        {
            return JsonForTree((await _organizationLogic.FindOrganizationTree(topOrg)).Data);
        }

        /// <summary>
        /// 读取树结构:排除下级
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Remark("用户控件-方法-读取组织机构树:排除下级", RemarkFrom.System)]
        [Route("/system/organization")]
        public async Task<JsonResult> Tree()
        {
            #region 获取权限控制器信息
            SystemOrganizationDataPermissionInput input = new SystemOrganizationDataPermissionInput
            {
                PrincipalUser = CurrentUser,
                DataSql = (await _permissionLogic.FindDataPermissionSql(new ViewRote
                {
                    UserId = CurrentUser.UserId,
                    MenuId = ResourceMenuId.组织架构.ToGuid()
                })).Data
            };
            #endregion
            return JsonForTree((await _organizationLogic.FindDataPermission(input)).Data);
        }

        /// <summary>
        /// 读取组织机构树
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("组织机构维护-方法-新增/编辑-读取组织机构树", RemarkFrom.System)]
        [Route("/system/organization/list")]
        public async Task<JsonResult> Find(SystemOrganizationDataPermissionInput input)
        {
            #region 获取权限控制器信息
            input.DataSql = (await _permissionLogic.FindDataPermissionSql(new ViewRote
            {
                UserId = CurrentUser.UserId,
                MenuId = ResourceMenuId.组织架构.ToGuid()
            })).Data;
            #endregion
            return JsonForGridPaging(await _organizationLogic.Find(input));
        }
        /// <summary>
        /// 读取树结构:排除下级
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Remark("用户控件-方法-读取组织机构树:排除下级", RemarkFrom.System)]
        [Route("/system/organization/removechildren/{id}")]
        public async Task<JsonResult> FindRemoveChildren([FromRoute] Guid? id = null)
        {
            SystemOrganizationDataPermissionInput input = new SystemOrganizationDataPermissionInput
            {
                PrincipalUser = CurrentUser,
                DataSql = (await _permissionLogic.FindDataPermissionSql(new ViewRote
                {
                    UserId = CurrentUser.UserId,
                    MenuId = ResourceMenuId.组织架构.ToGuid()
                })).Data
            };
            var orgs = (await _organizationLogic.FindDataPermission(input)).Data.ToList();
            if (id != null)
            {
                orgs = orgs.Where(w => !w.parents.Contains(id.ToString())).ToList();
            }
            return JsonForTree(orgs);
        }

        /// <summary>
        /// 保存组织机构数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("组织机构维护-方法-新增/编辑-保存", RemarkFrom.System)]
        [Route("/system/organization")]
        public async Task<JsonResult> Save(SystemOrganization input)
        {
            return Json(await _organizationLogic.Save(input));
        }

        /// <summary>
        /// 删除组织机构
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("组织机构维护-方法-列表-删除", RemarkFrom.System)]
        [Route("/system/organization/delete")]
        public async Task<JsonResult> Delete(IdInput<string> input)
        {
            return Json(await _organizationLogic.Delete(input));
        }
        /// <summary>
        /// 编辑/修改
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Remark("组织机构维护-方法-列表-根据id获取值", RemarkFrom.System)]
        [Route("/system/organization/{id}")]
        public async Task<JsonResult> FindById([FromRoute] IdInput input)
        {
            return Json(await _organizationLogic.FindById(input));
        }

        /// <summary>
        /// 冻结
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("组织机构维护-方法-冻结", RemarkFrom.System)]
        [Route("/system/organization/isfreeze")]
        public async Task<JsonResult> IsFreeze(IdInput input)
        {
            return Json(await _organizationLogic.IsFreeze(input));
        }
        #endregion
    }
}