﻿using EIP.Agile.Models.Dtos.Sn;
using EIP.Common.Controller.Attribute;
using EIP.System.Logic;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace EIP.System.Controller
{
    /// <summary>
    /// 
    /// </summary>
    public class SnController : BaseSystemController
    {
        #region 构造函数
        private readonly ISystemSnLogic _systemSnLogic;

        /// <summary>
        /// 系统快捷方式构造函数
        /// </summary>
        /// <param name="systemSnLogic"></param>
        public SnController(ISystemSnLogic systemSnLogic)
        {
            _systemSnLogic = systemSnLogic;
        }

        #endregion

        /// <summary>
        /// 获取规则值
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("规则-方法-获取规则值", RemarkFrom.System)]
        [Route("/system/sn")]
        public async Task<JsonResult> SaveUserRole(SystemSnInput input)
        {
            return Json(await _systemSnLogic.Find(input));
        }
    }
}
