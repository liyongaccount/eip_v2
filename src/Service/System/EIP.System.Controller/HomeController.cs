﻿/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Base.Models.Resx;
using EIP.Common.Controller.Attribute;
using EIP.Common.Core;
using EIP.Common.Models.Dtos;
using EIP.Common.Models;
using EIP.Common.Models.Tree;
using EIP.System.Logic;
using EIP.System.Models.Dtos.Permission;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EIP.System.Controller
{
    /// <summary>
    /// 主页
    /// </summary>
    public class HomeController : BaseSystemController
    {
        private readonly ISystemPermissionLogic _permissionLogic;
        private readonly ISystemLoginLogLogic _loginLogLogic;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="permissionLogic"></param>
        /// <param name="loginLogLogic"></param>
        public HomeController(ISystemPermissionLogic permissionLogic,
            ISystemLoginLogLogic loginLogLogic)
        {
            _loginLogLogic=loginLogLogic;
            _permissionLogic = permissionLogic;
        }

        /// <summary>
        /// 加载模块权限
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Remark("首页-方法-获取模块权限", RemarkFrom.System)]
        [Route("/system/home/menutree")]
        public async Task<JsonResult> MenusTree()
        {
            var permissions = (await _permissionLogic.MenusTree(new SystemPermissionMenuInput()
            {
                UserId = CurrentUser.UserId
            })).Data.ToList();
            List<BaseTree> baseTrees = new List<BaseTree>();
            foreach (var permission in permissions)
            {
                BaseTree baseTree = new BaseTree();
                baseTree.text = permission.Text;
                baseTree.parent = permission.ParentId;
                baseTree.id = permission.Id;
                baseTree.icon = permission.Icon;
                baseTrees.Add(baseTree);
            }
            return JsonForTree(baseTrees);
        }
        /// <summary>
        /// 获取当前登录人员所有菜单
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Remark("首页-方法-获取当前登录人员所有菜单", RemarkFrom.System)]
        [Route("/system/home/menu")]
        public async Task<JsonResult> Menus()
        {
            SystemPermissionMenuInput input = new SystemPermissionMenuInput
            {
                UserId = CurrentUser.UserId
            };
            return Json((await _permissionLogic.Menus(input)).Data.ToList());
        }

        /// <summary>
        /// 退出
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("主界面-方法-退出", RemarkFrom.System)]
        public async Task<JsonResult> LoginOut()
        {
            OperateStatus operateStatus = new OperateStatus();
            try
            {
                //修改时间
                if (CurrentUser != null)
                    await _loginLogLogic.LoginOut(new IdInput(CurrentUser.LoginId));
            }
            catch (EipException e)
            {
                operateStatus.Msg = e.Message;
                return Json(operateStatus);
            }
            operateStatus.Code = ResultCode.Success;
            operateStatus.Msg = ResourceSystem.退出成功;
            return Json(operateStatus);
        }
    }
}