﻿using System;

namespace EIP.System.Models.Dtos.MobileMenuButton
{
    /// <summary>
    /// 
    /// </summary>
    public class SystemMobileMenuButtonFindInput
    {
        /// <summary>
        /// 移动菜单
        /// </summary>
        public Guid MobileMenuId { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public Guid UserId { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class SystemMobileMenuButtonFindOutput
    {
        /// <summary>
        /// 菜单按钮id
        /// </summary>		
        public Guid MobileMenuButtonId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }


        /// <summary>
        /// 图标
        /// </summary>		
        public string Icon { get; set; }
        /// <summary>
        /// 背景图标
        /// </summary>		
		public string BgColor { get; set; }

        /// <summary>
        /// 图标图标
        /// </summary>		
		public string IconColor { get; set; }
        /// <summary>
        /// 执行方法
        /// </summary>		
		public string Method { get; set; }

        /// <summary>
        /// 自定义Script
        /// </summary>		
        public string Script { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        public int OrderNo { get; set; } = 0;

        /// <summary>
        /// 0Api,1方法
        /// </summary>
        public short Type { get; set; }

        /// <summary>
        /// Api地址
        /// </summary>
        public string ApiPath { get; set; }
    }
}
