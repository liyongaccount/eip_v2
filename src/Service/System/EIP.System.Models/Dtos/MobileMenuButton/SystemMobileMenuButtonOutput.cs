﻿using EIP.Base.Models.Entities.System;

namespace EIP.System.Models.Dtos.MobileMenuButton
{
    /// <summary>
    /// 
    /// </summary>
    public class SystemMobileMenuButtonOutput : SystemMobileMenuButton
    {
        /// <summary>
        /// 模块名称
        /// </summary>
        public string MenuNames { get; set; }
    }
}
