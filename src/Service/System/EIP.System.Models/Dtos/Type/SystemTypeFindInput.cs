﻿using EIP.Common.Models.Paging;
using System;

namespace EIP.System.Models.Dtos.Type
{
    /// <summary>
    /// 
    /// </summary>
    public class SystemTypeFindInput : QueryParam
    {
        /// <summary>
        /// 主键
        /// </summary>
        public Guid TypeId { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 是否冻结
        /// </summary>
        public bool IsFreeze { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int OrderNo { get; set; }
    }
}