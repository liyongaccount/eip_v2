using EIP.Common.Extension;
using EIP.Common.Models.Dtos;
using EIP.Common.Models.Paging;
using EIP.Common.Repository;
using EIP.System.Models.Dtos.MobileMenu;
using EIP.System.Models.Dtos.MobileMenuButton;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EIP.System.Repository.IRepository.Impl
{
    /// <summary>
    /// 移动端按钮
    /// </summary>
    public class SystemMobileMenuButtonRepository : ISystemMobileMenuButtonRepository
    {
        /// <summary>
        /// 根据模块获取功能项信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public Task<PagedResults<SystemMobileMenuFindButtonByMenuIdOutput>> FindMenuButtonByMenuId(SystemMobileMenuFindButtonByMenuIdInput input)
        {
            var sql = new StringBuilder();
            sql.Append("SELECT  menuButton.CreateTime,menuButton.CreateUserName,menuButton.UpdateTime,menuButton.UpdateUserName, menuButton.ApiPath,menuButton.Type,menuButton.MobileMenuButtonId,menuButton.MobileMenuName,menuButton.Icon,menuButton.Name,menuButton.Method,menuButton.OrderNo,menuButton.IsFreeze,menuButton.Remark,menu.ParentIdsName MenuNames,@rowNumber, @recordCount  FROM System_MobileMenuButton menuButton LEFT JOIN System_MobileMenu menu ON menu.MobileMenuId=menuButton.MobileMenuId @where ");

            if (input.Id.HasValue)
            {
                sql.Append($" AND menuButton.MobileMenuId='{input.Id}'");
            }

            if (input.Sidx.IsNullOrEmpty())
            {
                input.Sidx = " menuButton.OrderNo";
            }
            return new SqlMapperUtil().PagingQuerySqlAsync<SystemMobileMenuFindButtonByMenuIdOutput>(sql.ToString(), input);
        }

        /// <summary>
        /// 根据模块获取功能项信息
        /// </summary>
        /// <param name="menuId"></param>
        /// <returns></returns>
        public Task<IEnumerable<SystemMobileMenuButtonOutput>> FindMenuButtonByMenuId(IList<Guid> menuId = null)
        {
            var sql = new StringBuilder();
            sql.Append("SELECT menuButton.*,menu.ParentIdsName MenuNames FROM System_MobileMenuButton menuButton LEFT JOIN System_MobileMenu menu ON menu.MobileMenuId=menuButton.MobileMenuId  ");
            if (menuId != null && menuId.Any())
            {
                sql.Append($" WHERE menu.MobileMenuId in ({menuId.ExpandAndToString().InSql()})");
            }
            sql.Append(" order by menuButton.OrderNo asc");
            return new SqlMapperUtil().SqlWithParams<SystemMobileMenuButtonOutput>(sql.ToString());
        }

        /// <summary>
        /// 根据模块获取功能项信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public Task<IEnumerable<SystemMobileMenuButtonOutput>> FindHaveMenuButtonPermission(IdInput input)
        {
            var sql = new StringBuilder();
            sql.Append(@"SELECT button.*,menu.ParentIdsName MenuNames FROM System_MobileMenuButton button
            LEFT JOIN System_Permission per on MenuButtonId = per.PrivilegeAccessValue
            INNER JOIN System_PermissionUser perUser on per.PrivilegeMasterValue = perUser.PrivilegeMasterValue and PrivilegeMasterUserId=@userId and per.PrivilegeAccess = 1
            LEFT JOIN System_MobileMenu menu on menu.MobileMenuId = button.MobileMenuId
            ORDER BY button.OrderNo asc");
            return new SqlMapperUtil().SqlWithParams<SystemMobileMenuButtonOutput>(sql.ToString(), new
            {
                userId = input.Id
            });
        }

        /// <summary>
        /// 根据模块Id和用户Id获取按钮权限数据
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<SystemMobileMenuButtonFindOutput>> FindMobileMenuButtonByMenuIdAndUserId(SystemMobileMenuButtonFindInput input)
        {
            const string sql = @"
                        SELECT mb.MobileMenuButtonId,mb.Name,Script,Method,mb.Icon,mb.OrderNo,mb.IconColor,mb.BgColor,mb.ApiPath,mb.Type FROM System_MobileMenuButton mb
                        LEFT JOIN System_Permission per on MobileMenuButtonId=per.PrivilegeAccessValue
                        LEFT JOIN System_PermissionUser perUser on per.PrivilegeMasterValue=perUser.PrivilegeMasterValue
                        LEFT JOIN System_MobileMenu menu on menu.MobileMenuId=mb.MobileMenuId  
                        WHERE menu.MobileMenuId=@id and PrivilegeMasterUserId=@userId and per.PrivilegeAccess=6
                        GROUP BY mb.MobileMenuButtonId,mb.Name,Script,mb.Icon,mb.OrderNo,Method,mb.IconColor,mb.BgColor,mb.ApiPath,mb.Type
                        ORDER BY mb.OrderNo asc";
            return new SqlMapperUtil().SqlWithParams<SystemMobileMenuButtonFindOutput>(sql,
                new
                {
                    id = input.MobileMenuId,
                    userId = input.UserId
                });
        }
    }
}
