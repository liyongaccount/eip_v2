/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/

using EIP.Common.Extension;
using EIP.Common.Models.Paging;
using EIP.Common.Repository;
using EIP.System.Models.Dtos.Login;
using EIP.System.Models.Dtos.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EIP.System.Repository.Impl
{
    /// <summary>
    /// 系统人员管理
    /// </summary>
    public class SystemUserInfoRepository : ISystemUserInfoRepository
    {
        /// <summary>
        /// 复杂查询分页方式
        /// </summary>
        /// <param name="input">查询参数</param>
        /// <returns>分页</returns>
        public Task<PagedResults<SystemUserOutput>> Find(SystemUserPagingInput input)
        {
            var sql = new StringBuilder(
                 "SELECT userInfo.UserId," +
                 "userInfo.OrganizationId," +
                 "userInfo.Code," +
                 "userInfo.Name," +
                 "userInfo.Mobile," +
                 "userInfo.HeadImage," +
                 "userInfo.OtherContactInformation," +
                 "userInfo.Email," +
                 "userInfo.LastVisitTime," +
                 "userInfo.IsFreeze," +
                 "userInfo.Remark," +
                 "userInfo.Nature," +

                 "userInfo.CreateTime," +
                 "userInfo.CreateUserName," +
                 "userInfo.UpdateTime," +
                 "userInfo.UpdateUserName," +

                 "userInfo.UserOrganizationNames," +
                 "org.ParentIdsName,@rowNumber, @recordCount  " +
                 " FROM System_UserInfo userInfo LEFT JOIN System_PermissionUser perUser ON perUser.PrivilegeMasterUserId = userInfo.UserId " +
                 "LEFT JOIN System_Organization org on org.OrganizationId = userInfo.OrganizationId @where AND userInfo.IsAdmin=0 ");
            if (input.DataSql.IsNotNullOrEmpty())
            {
                sql.Append(@" AND " + input.DataSql + "");
            }
            sql.Append($@" AND perUser.PrivilegeMaster='{input.PrivilegeMaster.GetHashCode()}' and perUser.IsRelationOrganization='false'  ");

            if (!input.PrivilegeMasterValue.IsNullOrEmptyGuid())
            {
                sql.Append($@" AND perUser.PrivilegeMasterValue in (select OrganizationId from System_Organization where ParentIds like '%{input.PrivilegeMasterValue}%') ");
            }
            if (input.TopOrg.IsNotNullOrEmpty())
            {
                sql.Append("AND org.ParentIds  like '%" + input.TopOrg.Xss().FilterSql() + "%'");
            }
            if (input.Sidx.IsNullOrEmpty())
            {
                input.Sidx = " userInfo.OrganizationId ";
            }
            sql.Append(" GROUP BY userInfo.UserOrganizationNames,userInfo.HeadImage,userInfo.UserId,userInfo.OrganizationId,userInfo.Code,userInfo.Name,userInfo.Mobile,userInfo.OtherContactInformation,userInfo.Email,userInfo.LastVisitTime,userInfo.IsFreeze,userInfo.Remark,userInfo.Nature,org.ParentIdsName");
            return new SqlMapperUtil().PagingQuerySqlAsync<SystemUserOutput>(sql.ToString(), input);
        }
        /// <summary>
        /// 复杂查询分页方式
        /// </summary>
        /// <param name="input">查询参数</param>
        /// <returns>分页</returns>
        public Task<IEnumerable<SystemUserFindCommonOutput>> FindCommon(SystemUserFindCommonInput input)
        {
            var sql = new StringBuilder(
                 "SELECT userInfo.UserId," +
                 "userInfo.OrganizationId," +
                 "userInfo.Name," +
                 "userInfo.Sex," +
                 "userInfo.Code," +
                 "org.ParentIdsName  " +
                 " FROM System_UserInfo userInfo" +
                 " LEFT JOIN System_PermissionUser perUser ON perUser.PrivilegeMasterUserId = userInfo.UserId " +
                 " LEFT JOIN System_Organization org on org.OrganizationId = userInfo.OrganizationId where  userInfo.IsAdmin=0 ");
            if (input.DataSql.IsNotNullOrEmpty())
            {
                sql.Append(@" AND " + input.DataSql + "");
            }

            if (input.PrivilegeMaster.HasValue)
            {
                sql.Append($@" AND perUser.PrivilegeMaster='{input.PrivilegeMaster.GetHashCode()}' ");
            }

            if (!input.PrivilegeMasterValue.IsNullOrEmptyGuid())
            {
                sql.Append($@" AND perUser.PrivilegeMasterValue in (select OrganizationId from System_Organization where ParentIds like '%{input.PrivilegeMasterValue}%') ");
            }

            sql.Append(" GROUP BY userInfo.Code,userInfo.Sex,userInfo.UserId,userInfo.OrganizationId,userInfo.Name,org.ParentIdsName");
            return new SqlMapperUtil().SqlWithParams<SystemUserFindCommonOutput>(sql.ToString());
        }
        /// <summary>
        /// 复杂查询分页方式
        /// </summary>
        /// <param name="dataSql">查询参数</param>
        /// <returns>分页</returns>
        public Task<IEnumerable<SystemUserChosenOutput>> FindAllUser(string dataSql)
        {
            var sql = new StringBuilder(
                 "SELECT userInfo.UserId," +
                 "userInfo.OrganizationId," +
                 "userInfo.Code," +
                 "userInfo.Name," +
                 "org.ParentIdsName OrganizationName FROM System_UserInfo userInfo LEFT JOIN System_Organization org on org.OrganizationId = userInfo.OrganizationId where 1=1 ");
            if (dataSql.IsNotNullOrEmpty())
            {
                sql.Append(@" AND " + dataSql + "");
            }
            sql.Append($@" AND userInfo.IsAdmin=0 ");

            sql.Append(" GROUP BY userInfo.HeadImage,userInfo.UserId,userInfo.OrganizationId,userInfo.Code,userInfo.Name,userInfo.Mobile,userInfo.OtherContactInformation,userInfo.Email,userInfo.LastVisitTime,userInfo.IsFreeze,userInfo.Remark,userInfo.Nature,userInfo.OrganizationId,org.ParentIdsName");
            return new SqlMapperUtil().SqlWithParams<SystemUserChosenOutput>(sql.ToString());
        }

        #region 登录
        /// <summary>
        /// 根据登录代码和密码查询用户信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public Task<SystemLoginOutput> Login(SystemLoginInput input)
        {
            string sql = "SELECT UserId,OrganizationId,OrganizationName,Code,Name ,IsFreeze,IsAdmin,HeadImage  FROM System_UserInfo where Code=@code and Password=@password";
            return new SqlMapperUtil().SqlWithParamsSingle<SystemLoginOutput>(sql, new
            {
                code = input.Code,
                password = input.Password
            });
        }

        /// <summary>
        /// 更新最后时间
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <returns></returns>
        public Task<bool> UpdateTime(Guid userId)
        {
            string sql = "UPDATE System_UserInfo SET LastVisitTime=@time WHERE UserId=@userId ";
            return new SqlMapperUtil().InsertUpdateOrDeleteSqlBool<bool>(sql, new
            {
                time = DateTime.Now,
                userId
            });
        }
        #endregion
    }
}