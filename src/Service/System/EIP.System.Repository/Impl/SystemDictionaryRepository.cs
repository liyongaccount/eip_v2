using EIP.Common.Extension;
using EIP.Common.Models.Paging;
using EIP.Common.Repository;
using EIP.System.Models.Dtos.Dictionary;
using System.Text;
using System.Threading.Tasks;

namespace EIP.System.Repository.Impl
{
    /// <summary>
    /// 字典数据访问接口实现
    /// </summary>
    public class SystemDictionaryRepository : ISystemDictionaryRepository
    {
        /// <summary>
        /// 根据字典代码获取对应下级值
        /// </summary>
        /// <param name="input">代码值</param>
        /// <returns></returns>
        public Task<PagedResults<SystemDictionaryFindOutput>> Find(SystemDictionaryFindInput input)
        {
            var sql = new StringBuilder();
            sql.Append("select *,@rowNumber, @recordCount from System_Dictionary dic @where ");
            if (input.Id.HasValue)
            {
                sql.Append(input.HaveSelf
                    ? "AND dic.ParentIds  like '%" + input.Id + "%'"
                    : "AND dic.ParentIds  like '%" + input.Id + ",%'");
            }
            else
            {
                sql.Append($" and dic.ParentId is null ");
            }
            if (input.Sidx.IsNullOrEmpty())
            {
                input.Sidx = " dic.OrderNo";
            }
            return new SqlMapperUtil().PagingQuerySqlAsync<SystemDictionaryFindOutput>(sql.ToString(), input);
        }
    }
}