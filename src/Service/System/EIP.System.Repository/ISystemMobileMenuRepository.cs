using EIP.Common.Models.Paging;
using EIP.System.Models.Dtos.MobileMenu;
using System.Threading.Tasks;

namespace EIP.System.Repository.IRepository
{
    /// <summary>
    /// 移动端菜单
    /// </summary>
    public interface ISystemMobileMenuRepository
    {
        /// <summary>
        /// 根据父级获取下面模块
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PagedResults<SystemMobileMenuFindOutput>> Find(SystemMobileMenuFindInput input);
    }
}
