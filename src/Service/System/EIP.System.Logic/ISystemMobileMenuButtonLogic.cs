using EasyCaching.Core.Interceptor;
using EIP.Base.Models.Entities.System;
using EIP.Common.Logic;
using EIP.Common.Models;
using EIP.Common.Models.Dtos;
using EIP.Common.Models.Paging;
using EIP.System.Models.Dtos.MobileMenu;
using EIP.System.Models.Dtos.Permission;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using System.Threading.Tasks;
namespace EIP.System.Logic.Permission.ILogic
{
    /// <summary>
    /// 移动端按钮
    /// </summary>
    public interface ISystemMobileMenuButtonLogic : IAsyncLogic<SystemMobileMenuButton>
    {
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="input">移动端按钮</param>
        /// <returns></returns>
        [EasyCachingEvict(IsAll = true, CacheKeyPrefix = "ISystemMobileMenuButtonLogic_Cache")]
        Task<OperateStatus> Save(SystemMobileMenuButton input);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [EasyCachingEvict(IsAll = true, CacheKeyPrefix = "ISystemMobileMenuButtonLogic_Cache")]
        Task<OperateStatus> Delete(IdInput<string> input);

        /// <summary>
        /// 根据模块获取功能项信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [EasyCachingAble(CacheKeyPrefix = "ISystemMobileMenuButtonLogic_Cache")]
        Task<OperateStatus<PagedResults<SystemMobileMenuFindButtonByMenuIdOutput>>> FindMenuButtonByMenuId(SystemMobileMenuFindButtonByMenuIdInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [EasyCachingAble(CacheKeyPrefix = "ISystemMobileMenuButtonLogic_Cache")]
        Task<OperateStatus<SystemMobileMenuButton>> FindById(IdInput input);

        /// <summary>
        /// 冻结
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [EasyCachingEvict(IsAll = true, CacheKeyPrefix = "ISystemMobileMenuButtonLogic_Cache")]
        Task<OperateStatus> IsFreeze(IdInput input);

    }
}
