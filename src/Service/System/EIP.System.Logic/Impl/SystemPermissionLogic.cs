/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Base.Models.Entities.System;
using EIP.Base.Repository.Fixture;
using EIP.Common.Core.Context;
using EIP.Common.Extension;
using EIP.Common.Language.Resource;
using EIP.Common.Logic;
using EIP.Common.Models;
using EIP.Common.Models.Dtos;
using EIP.Common.Models.Tree;
using EIP.System.Models.Dtos.Data;
using EIP.System.Models.Dtos.MenuButton;
using EIP.System.Models.Dtos.MobileMenuButton;
using EIP.System.Models.Dtos.Permission;
using EIP.System.Models.Enums;
using EIP.System.Repository;
using EIP.System.Repository.IRepository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EIP.System.Logic.Impl
{
    /// <summary>
    /// 权限业务逻辑
    /// </summary>
    public class SystemPermissionLogic : DapperAsyncLogic<SystemPermission>, ISystemPermissionLogic
    {
        #region 依赖注入
        private readonly ISystemMobileMenuButtonRepository _systemMobileMenuButtonRepository;
        private readonly ISystemMenuButtonRepository _menuButtonRepository;
        private readonly ISystemPermissionRepository _permissionRepository;
        private readonly ISystemDataRepository _dataRepository;
        /// <summary>
        /// 权限逻辑
        /// </summary>
        /// <param name="menuButtonRepository"></param>
        /// <param name="permissionRepository"></param>
        /// <param name="dataRepository"></param>
        public SystemPermissionLogic(
             ISystemMobileMenuButtonRepository systemMobileMenuButtonRepository,
            ISystemMenuButtonRepository menuButtonRepository,
            ISystemPermissionRepository permissionRepository,
            ISystemDataRepository dataRepository)
        {
            _menuButtonRepository = menuButtonRepository;
            _permissionRepository = permissionRepository;
            _dataRepository = dataRepository;
            _systemMobileMenuButtonRepository = systemMobileMenuButtonRepository;
        }
        #endregion

        #region 方法

        /// <summary>
        /// 根据状态为True的模块信息
        /// </summary>
        /// <returns></returns>
        public async Task<OperateStatus<IEnumerable<SystemPermission>>> FindPermissionByPrivilegeMasterValue(SystemPermissionByPrivilegeMasterValueInput input)
        {
            return OperateStatus<IEnumerable<SystemPermission>>.Success((await _permissionRepository.FindPermissionByPrivilegeMasterValue(input)).ToList());
        }

        /// <summary>
        /// 根据用户Id获取用户具有的移动端模块权限
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<OperateStatus<IEnumerable<SystemMobileMenu>>> FindSystemPermissionMobileMenuByUserId()
        {
            var user = EipHttpContext.CurrentUser();
            using (var fixture = new SqlDatabaseFixture())
            {
                try
                {
                    IList<SystemMobileMenu> mobileMenus = new List<SystemMobileMenu>();
                    if (user.IsAdmin)
                    {
                        return OperateStatus<IEnumerable<SystemMobileMenu>>.Success((await fixture.Db.SystemMobileMenu.FindAllAsync(f => !f.IsFreeze)).OrderBy(o => o.OrderNo));
                    }
                    return OperateStatus<IEnumerable<SystemMobileMenu>>.Success(await _permissionRepository.FindSystemPermissionMobileMenuByUserId(user.UserId));
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        /// <summary>
        /// 保存权限信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<OperateStatus> SavePermission(SystemPermissionSaveInput input)
        {
            using (var fixture = new SqlDatabaseFixture(false))
            {
                var operateStatus = new OperateStatus();
                try
                {
                    IList<SystemPermission> systemPermissions = input.Permissiones.Select(per => new SystemPermission
                    {
                        PrivilegeAccess = input.PrivilegeAccess.ToShort(),
                        PrivilegeAccessValue = per,
                        PrivilegeMasterValue = input.PrivilegeMasterValue,
                        PrivilegeMaster = input.PrivilegeMaster.ToShort(),
                        PrivilegeMenuId = input.PrivilegeMenuId
                    }).ToList();

                    if (!input.PrivilegeMenuId.IsNullOrEmptyGuid())
                    {
                        await fixture.Db.SystemPermission.DeleteAsync(f => f.PrivilegeMasterValue == input.PrivilegeMasterValue && f.PrivilegeAccess == input.PrivilegeAccess.ToShort() && f.PrivilegeMenuId == input.PrivilegeMenuId);
                    }
                    else
                    {
                        await fixture.Db.SystemPermission.DeleteAsync(f => f.PrivilegeMasterValue == input.PrivilegeMasterValue && f.PrivilegeAccess == input.PrivilegeAccess.ToShort());
                    }

                    if (input.PrivilegeMaster == EnumPrivilegeMaster.人员)
                    {
                        var master = input.PrivilegeMaster.ToShort();
                        //删除对应人员数据
                        await fixture.Db.SystemPermissionUser.DeleteAsync(d => d.PrivilegeMaster == master && d.PrivilegeMasterValue == input.PrivilegeMasterValue);
                        //判断是否具有权限
                        if (!systemPermissions.Any())
                        {
                            operateStatus.Code = ResultCode.Success;
                            operateStatus.Msg = Chs.Successful;
                            return operateStatus;
                        }

                        //插入权限人员数据
                        var permissionUser = new SystemPermissionUser
                        {
                            PrivilegeMaster = (byte)input.PrivilegeMaster,
                            PrivilegeMasterUserId = input.PrivilegeMasterValue,
                            PrivilegeMasterValue = input.PrivilegeMasterValue
                        };
                        await fixture.Db.SystemPermissionUser.InsertAsync(permissionUser);
                    }

                    //是否具有权限数据
                    if (!systemPermissions.Any())
                    {
                        operateStatus.Code = ResultCode.Success;
                        operateStatus.Msg = Chs.Successful;
                        return operateStatus;
                    }

                    //插入数据库
                    await fixture.Db.SystemPermission.BulkInsertAsync(systemPermissions);
                    operateStatus.Code = ResultCode.Success;
                    operateStatus.Msg = Chs.Successful;
                    return operateStatus;
                }
                catch (Exception ex)
                {
                    operateStatus.Msg = ex.Message;
                    return operateStatus;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<OperateStatus<IEnumerable<SystemPermissionFindMenuByUserIdOutput>>> MenusTree(SystemPermissionMenuInput input)
        {
            IList<SystemPermissionFindMenuByUserIdOutput> treeEntities = new List<SystemPermissionFindMenuByUserIdOutput>();
            //判断该用户是否为超级管理员:若是超级管理员则显示所有模块
            var userInfo = EipHttpContext.CurrentUser();
            //如果是超级管理员
            if (userInfo.IsAdmin)
            {
                treeEntities = (await _permissionRepository.FindSystemPermissionMenuByAdmin()).ToList();
            }
            else
            {
                treeEntities = (await _permissionRepository.FindSystemPermissionMenuByUserId(input)).ToList();
            }
            return OperateStatus<IEnumerable<SystemPermissionFindMenuByUserIdOutput>>.Success(treeEntities);
        }
        /// <summary>
        /// 根据用户Id获取用户具有的模块权限
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<OperateStatus<IEnumerable<SystemPermissionFindMenuByUserIdRouterOutput>>> Menus(SystemPermissionMenuInput input)
        {
            try
            {
                List<SystemPermissionFindMenuByUserIdRouterOutput> outputs = new List<SystemPermissionFindMenuByUserIdRouterOutput>();
                IList<SystemPermissionFindMenuByUserIdOutput> treeEntities = new List<SystemPermissionFindMenuByUserIdOutput>();
                //判断该用户是否为超级管理员:若是超级管理员则显示所有模块
                var userInfo = EipHttpContext.CurrentUser();
                //如果是超级管理员
                if (userInfo.IsAdmin)
                {
                    treeEntities = (await _permissionRepository.FindSystemPermissionMenuByAdmin()).ToList();
                }
                else
                {
                    treeEntities = (await _permissionRepository.FindSystemPermissionMenuByUserId(input)).ToList();
                }
                //解析路由方式
                if (treeEntities.Any())
                {
                    foreach (var data in treeEntities.Where(w => w.ParentId == null))
                    {
                        SystemPermissionFindMenuByUserIdRouterOutput output = new SystemPermissionFindMenuByUserIdRouterOutput
                        {
                            router = data.Router,
                            name = data.Text,
                            path = data.Path.IsNullOrEmpty() ? "" : data.Path,
                            meta = new SystemPermissionFindMenuByUserIdRouterMetaOutput
                            {
                                menuId = data.Id,
                                icon = data.Icon,
                                theme = data.Theme,
                                @params = data.Params.IsNotNullOrEmpty() ? JsonConvert.DeserializeObject(data.Params) : null,
                                link = data.OpenType == 2 ? data.Path : ""
                            }
                        };
                        output.children = FindChildren(treeEntities.ToList(), data);
                        outputs.Add(output);
                    }
                }
                return OperateStatus<IEnumerable<SystemPermissionFindMenuByUserIdRouterOutput>>.Success(outputs);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 递归获取树形结构
        /// </summary>
        /// <param name="datas"></param>
        /// <param name="treeEntity"></param>
        private List<SystemPermissionFindMenuByUserIdRouterOutput> FindChildren(List<SystemPermissionFindMenuByUserIdOutput> datas, SystemPermissionFindMenuByUserIdOutput treeEntity)
        {
            List<SystemPermissionFindMenuByUserIdRouterOutput> treeEntities = new List<SystemPermissionFindMenuByUserIdRouterOutput>();
            var children = datas.Where(w => w.ParentId != null && w.ParentId.ToString() == treeEntity.Id.ToString());
            foreach (var item in children)
            {
                SystemPermissionFindMenuByUserIdRouterOutput treeEntityChidren = new SystemPermissionFindMenuByUserIdRouterOutput
                {
                    router = item.Router,
                    name = item.Text,
                    path = item.Path.IsNullOrEmpty() ? "" : item.Path,
                    meta = new SystemPermissionFindMenuByUserIdRouterMetaOutput
                    {
                        menuId = item.Id,
                        icon = item.Icon,
                        theme = item.Theme,
                        @params = item.Params.IsNotNullOrEmpty() ? JsonConvert.DeserializeObject(item.Params) : null,
                        link = item.OpenType == 2 ? item.Path : ""
                    }
                };
                treeEntityChidren.children = FindChildren(datas, item);
                treeEntities.Add(treeEntityChidren);
            }
            return treeEntities;
        }

        /// <summary>
        /// 根据用户Id获取用户具有的模块权限
        /// </summary>
        /// <returns></returns>
        public async Task<OperateStatus<IEnumerable<BaseTree>>> FindSystemPermissionMenuAllByUserId()
        {
            var userInfo = EipHttpContext.CurrentUser();
            using (var fixture = new SqlDatabaseFixture())
            {
                try
                {
                    //如果是超级管理员
                    if (userInfo.IsAdmin)
                    {
                        return OperateStatus<IEnumerable<BaseTree>>.Success((await fixture.Db.SystemMenu.FindAllAsync(f => !f.IsFreeze && f.HaveMenuPermission)).OrderBy(o => o.OrderNo).MapToList<SystemMenu, BaseTree>());
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return OperateStatus<IEnumerable<BaseTree>>.Success((await _permissionRepository.FindSystemPermissionMenuAllByUserId(userInfo.UserId)).ToList());
        }
        /// <summary>
        /// 根据用户Id获取用户具有的模块权限
        /// </summary>
        /// <returns></returns>
        public async Task<OperateStatus<IEnumerable<BaseTree>>> FindSystemPermissionMobileMenuAllByUserId()
        {
            var userInfo = EipHttpContext.CurrentUser();
            using (var fixture = new SqlDatabaseFixture())
            {
                try
                {
                    //如果是超级管理员
                    if (userInfo.IsAdmin)
                    {
                        return OperateStatus<IEnumerable<BaseTree>>.Success((await fixture.Db.SystemMobileMenu.FindAllAsync(f => !f.IsFreeze && f.HaveMenuPermission)).OrderBy(o => o.OrderNo).MapToList<SystemMobileMenu, BaseTree>());
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return OperateStatus<IEnumerable<BaseTree>>.Success((await _permissionRepository.FindSystemPermissionMobileMenuAllByUserId(userInfo.UserId)).ToList());
        }

        /// <summary>
        /// 根据角色Id,岗位Id,组Id,人员Id获取具有的模块信息
        /// </summary>
        /// <param name="input">输入参数</param>
        /// <returns>树形模块信息</returns>
        public async Task<OperateStatus<IEnumerable<BaseTree>>> FindMenuHavePermissionByPrivilegeMasterValue(SystemPermissiontMenuHaveByPrivilegeMasterValueInput input)
        {
            return OperateStatus<IEnumerable<BaseTree>>.Success((await _permissionRepository.FindMenuHavePermissionByPrivilegeMasterValue(input)).ToList());
        }
        /// <summary>
        /// 根据角色Id,岗位Id,组Id,人员Id获取具有的模块信息
        /// </summary>
        /// <param name="input">输入参数</param>
        /// <returns>树形模块信息</returns>
        public async Task<OperateStatus<IEnumerable<BaseTree>>> FindMobileMenuHavePermissionByPrivilegeMasterValue(SystemPermissiontMenuHaveByPrivilegeMasterValueInput input)
        {
            return OperateStatus<IEnumerable<BaseTree>>.Success((await _permissionRepository.FindMobileMenuHavePermissionByPrivilegeMasterValue(input)).ToList());
        }
        /// <summary>
        /// 查询对应拥有的功能项模块信息
        /// </summary>
        /// <returns></returns>
        public async Task<OperateStatus<IEnumerable<SystemPermissionFindMenuButtonByPrivilegeMasterOutput>>> FindMenuButtonByPrivilegeMaster(SystemPermissionFindMenuButtonByPrivilegeMasterInput input)
        {
            IList<SystemPermissionFindMenuButtonByPrivilegeMasterOutput> systemMenuButtons = new List<SystemPermissionFindMenuButtonByPrivilegeMasterOutput>();

            //当前传递的人员,角色,组织等拥有的菜单
            var menus = (await _permissionRepository.FindMenuHavePermissionByPrivilegeMasterValue(new SystemPermissiontMenuHaveByPrivilegeMasterValueInput()
            {
                PrivilegeMaster = input.PrivilegeMaster,
                PrivilegeMasterValue = input.PrivilegeMasterValue,
                PrivilegeAccess = EnumPrivilegeAccess.模块按钮
            })).ToList();

            if (menus.Any())
            {
                //当前传递的人员,角色,组织等拥有的按钮
                IList<SystemPermission> haveFunctions = (await FindPermissionByPrivilegeMasterValue(new SystemPermissionByPrivilegeMasterValueInput
                {
                    PrivilegeAccess = EnumPrivilegeAccess.模块按钮,
                    PrivilegeMasterValue = input.PrivilegeMasterValue,
                    PrivilegeMaster = input.PrivilegeMaster
                })).Data.ToList();
                IList<SystemMenuButtonOutput> menuButtons = new List<SystemMenuButtonOutput>();
                //判断该用户是否为超级管理员:若是超级管理员则显示所有按钮
                var userInfo = EipHttpContext.CurrentUser();
                //如果是超级管理员
                if (userInfo.IsAdmin)
                {
                    menuButtons = (await _menuButtonRepository.FindMenuButtonByMenuId(menus.Select(s => (Guid)s.id).ToList())).ToList();
                }
                else
                {
                    menuButtons = (await _menuButtonRepository.FindHaveMenuButtonPermission(new IdInput(input.UserId))).ToList();
                }
                //当前人员拥有的按钮
                foreach (var menu in menus)
                {
                    var menuButton = menuButtons.Where(w => w.MenuId == (Guid)menu.id).OrderBy(o => o.OrderNo);
                    foreach (var button in menuButton)
                    {
                        var selectFunction = haveFunctions.Where(w => w.PrivilegeAccessValue == button.MenuButtonId);
                        systemMenuButtons.Add(new SystemPermissionFindMenuButtonByPrivilegeMasterOutput
                        {
                            Exist = selectFunction.Any(),
                            MenuId = button.MenuId,
                            Name = button.Name,
                            Icon = button.Icon,
                            MenuNames = button.MenuNames,
                            MenuButtonId=button.MenuButtonId,
                        });
                    }
                }
            }
            return OperateStatus<IEnumerable<SystemPermissionFindMenuButtonByPrivilegeMasterOutput>>.Success(systemMenuButtons);
        }

        /// <summary>
        /// 查询对应拥有的功能项模块信息
        /// </summary>
        /// <returns></returns>
        public async Task<OperateStatus<IEnumerable<SystemPermissionFindMenuButtonByPrivilegeMasterOutput>>> FindMobileMenuButtonByPrivilegeMaster(SystemPermissionFindMenuButtonByPrivilegeMasterInput input)
        {
            IList<SystemPermissionFindMenuButtonByPrivilegeMasterOutput> systemMenuButtons = new List<SystemPermissionFindMenuButtonByPrivilegeMasterOutput>();

            //当前传递的人员,角色,组织等拥有的菜单
            var menus = (await _permissionRepository.FindMobileMenuHavePermissionByPrivilegeMasterValue(new SystemPermissiontMenuHaveByPrivilegeMasterValueInput()
            {
                PrivilegeMaster = input.PrivilegeMaster,
                PrivilegeMasterValue = input.PrivilegeMasterValue,
                PrivilegeAccess = EnumPrivilegeAccess.移动端模块按钮
            })).ToList();

            if (menus.Any())
            {
                //当前传递的人员,角色,组织等拥有的按钮
                IList<SystemPermission> haveFunctions = (await FindPermissionByPrivilegeMasterValue(new SystemPermissionByPrivilegeMasterValueInput
                {
                    PrivilegeAccess = EnumPrivilegeAccess.移动端模块按钮,
                    PrivilegeMasterValue = input.PrivilegeMasterValue,
                    PrivilegeMaster = input.PrivilegeMaster
                })).Data.ToList();
                IList<SystemMobileMenuButtonOutput> menuButtons = new List<SystemMobileMenuButtonOutput>();
                //当前人员是否为超级管理员
                using (var fixture = new SqlDatabaseFixture())
                {
                    //判断该用户是否为超级管理员:若是超级管理员则显示所有按钮
                    var userInfo = EipHttpContext.CurrentUser();
                    //如果是超级管理员
                    if (userInfo.IsAdmin)
                    {
                        menuButtons = (await _systemMobileMenuButtonRepository.FindMenuButtonByMenuId(menus.Select(s => (Guid)s.id).ToList())).ToList();
                    }
                    else
                    {
                        menuButtons = (await _systemMobileMenuButtonRepository.FindHaveMenuButtonPermission(new IdInput(input.UserId))).ToList();
                    }
                }
                //当前人员拥有的按钮
                foreach (var menu in menus)
                {
                    var menuButton = menuButtons.Where(w => w.MobileMenuId == (Guid)menu.id).OrderBy(o => o.OrderNo);
                    foreach (var button in menuButton)
                    {
                        var selectFunction = haveFunctions.Where(w => w.PrivilegeAccessValue == button.MobileMenuButtonId);
                        systemMenuButtons.Add(new SystemPermissionFindMenuButtonByPrivilegeMasterOutput
                        {
                            Exist = selectFunction.Any(),
                            MenuId = button.MobileMenuId,
                            Name = button.Name,
                            Icon = button.Icon,
                            MenuNames = button.MenuNames,
                            MenuButtonId = button.MobileMenuButtonId,
                        });
                    }
                }
            }
            return OperateStatus<IEnumerable<SystemPermissionFindMenuButtonByPrivilegeMasterOutput>>.Success(systemMenuButtons);
        }
        /// <summary>
        /// 查询对应拥有的功能项模块信息
        /// </summary>
        /// <returns></returns>
        public async Task<OperateStatus<IEnumerable<SystemPermissionFindDataByPrivilegeMasterOutput>>> FindDataByPrivilegeMaster(SystemPermissionFindDataByPrivilegeMasterInput input)
        {
            IList<SystemPermissionFindDataByPrivilegeMasterOutput> datas = new List<SystemPermissionFindDataByPrivilegeMasterOutput>();

            //获取拥有的模块信息
            var menus = (await FindMenuHavePermissionByPrivilegeMasterValue(new SystemPermissiontMenuHaveByPrivilegeMasterValueInput
            {
                PrivilegeMaster = input.PrivilegeMaster,
                PrivilegeMasterValue = input.PrivilegeMasterValue,
                PrivilegeAccess = EnumPrivilegeAccess.数据权限
            })).Data.ToList();

            if (menus.Any())
            {
                //获取拥有的功能项信息
                IList<SystemPermission> haveDatas = (await FindPermissionByPrivilegeMasterValue(new SystemPermissionByPrivilegeMasterValueInput
                {
                    PrivilegeAccess = EnumPrivilegeAccess.数据权限,
                    PrivilegeMaster = input.PrivilegeMaster,
                    PrivilegeMasterValue = input.PrivilegeMasterValue
                })).Data.ToList();

                IList<SystemDataOutput> dataOutputs = new List<SystemDataOutput>();
                //当前人员是否为超级管理员

                //判断该用户是否为超级管理员:若是超级管理员则显示所有按钮
                var userInfo = EipHttpContext.CurrentUser();
                //如果是超级管理员
                if (userInfo.IsAdmin)
                {
                    dataOutputs = (await _dataRepository.FindDataByMenuId(menus.Select(s => (Guid)s.id).ToList())).ToList();
                }
                else
                {
                    dataOutputs = (await _dataRepository.FindHaveDataPermission(new IdInput(input.UserId))).ToList();
                }
                foreach (var menu in menus)
                {
                    var data = dataOutputs.Where(w => w.MenuId == (Guid)menu.id).OrderBy(o => o.OrderNo);
                    foreach (var d in data)
                    {
                        var selectFunction = haveDatas.Where(w => w.PrivilegeAccessValue == d.DataId);
                        datas.Add(new SystemPermissionFindDataByPrivilegeMasterOutput
                        {
                            DataId= d.DataId,
                            Name=d.Name,
                            MenuNames=d.MenuNames,
                            MenuId=d.MenuId,
                            Exist= selectFunction.Any()
                        });
                    }
                }
            }
            return OperateStatus<IEnumerable<SystemPermissionFindDataByPrivilegeMasterOutput>>.Success(datas);
        }

        /// <summary>
        /// 查询对应拥有的功能项模块信息
        /// </summary>
        /// <returns></returns>
        public async Task<OperateStatus<IEnumerable<SystemMenuButton>>> FindMenuButtonByPrivilegeMasterAll(SystemPermissionFindMenuButtonByPrivilegeMasterInput input)
        {
            IList<SystemMenuButton> systemMenuButtons = new List<SystemMenuButton>();
            List<BaseTree> menus;
            IList<SystemPermission> haveFunctions = (await FindPermissionByPrivilegeMasterValue(new SystemPermissionByPrivilegeMasterValueInput
            {
                PrivilegeAccess = EnumPrivilegeAccess.模块按钮,
                PrivilegeMasterValue = input.PrivilegeMasterValue,
                PrivilegeMaster = input.PrivilegeMaster
            })).Data.ToList();
            IList<SystemMenuButtonOutput> menuButtons;
            var userInfo = EipHttpContext.CurrentUser();
            //如果是超级管理员
            if (userInfo.IsAdmin)
            {
                using (var fix = new SqlDatabaseFixture())
                {
                    menus = (await fix.Db.SystemMenu.FindAllAsync(f => !f.IsFreeze && f.HaveMenuPermission)).OrderBy(o => o.OrderNo).MapToList<SystemMenu, BaseTree>();
                }
                menuButtons = (await _menuButtonRepository.FindMenuButtonByMenuId()).ToList();
            }
            else
            {
                menus = (await FindSystemPermissionMenuAllByUserId()).Data.ToList();
                menuButtons = (await _menuButtonRepository.FindHaveMenuButtonPermission(new IdInput(input.UserId))).ToList();
            }
            //当前人员拥有的按钮
            foreach (var menu in menus)
            {
                var menuButton = menuButtons.Where(w => w.MenuId == (Guid)menu.id).OrderBy(o => o.OrderNo);
                foreach (var button in menuButton)
                {
                    var selectFunction = haveFunctions.Where(w => w.PrivilegeAccessValue == button.MenuButtonId);
                    button.Remark = selectFunction.Any() ? "selected" : "";
                    systemMenuButtons.Add(button);
                }
            }
            return OperateStatus<IEnumerable<SystemMenuButton>>.Success(systemMenuButtons);
        }

        /// <summary>
        /// 查询对应拥有的功能项模块信息
        /// </summary>
        /// <returns></returns>
        public async Task<OperateStatus<IEnumerable<SystemData>>> FindDataByPrivilegeMasterAll(SystemPermissionFindDataByPrivilegeMasterInput input)
        {
            IList<SystemData> datas = new List<SystemData>();

            //获取拥有的功能项信息
            IList<SystemPermission> haveDatas = (await FindPermissionByPrivilegeMasterValue(new SystemPermissionByPrivilegeMasterValueInput
            {
                PrivilegeAccess = EnumPrivilegeAccess.数据权限,
                PrivilegeMaster = input.PrivilegeMaster,
                PrivilegeMasterValue = input.PrivilegeMasterValue
            })).Data.ToList();
            List<BaseTree> menus;
            IList<SystemDataOutput> dataOutputs;

            var userInfo = EipHttpContext.CurrentUser();
            //如果是超级管理员
            if (userInfo.IsAdmin)
            {
                using (var fix = new SqlDatabaseFixture())
                {
                    menus = (await fix.Db.SystemMenu.FindAllAsync(f => !f.IsFreeze && f.HaveMenuPermission)).OrderBy(o => o.OrderNo).MapToList<SystemMenu, BaseTree>();
                }
                dataOutputs = (await _dataRepository.FindDataByMenuId()).ToList();
            }
            else
            {
                menus = (await FindSystemPermissionMenuAllByUserId()).Data.ToList();
                dataOutputs = (await _dataRepository.FindHaveDataPermission(new IdInput(input.UserId))).ToList();
            }
            foreach (var menu in menus)
            {
                var data = dataOutputs.Where(w => w.MenuId == (Guid)menu.id).OrderBy(o => o.OrderNo);
                foreach (var d in data)
                {
                    var selectFunction = haveDatas.Where(w => w.PrivilegeAccessValue == d.DataId);
                    d.Remark = selectFunction.Any() ? "selected" : "";
                    datas.Add(d);
                }
            }
            return OperateStatus<IEnumerable<SystemData>>.Success(datas);
        }

        /// <summary>
        /// 获取登录人员对应模块下的功能项
        /// </summary>
        /// <param name="viewRote">路由信息</param>
        /// <returns></returns>
        public async Task<OperateStatus<IEnumerable<SystemMenuButtonByViewRote>>> FindMenuButton(ViewRote viewRote)
        {
            using (var fixture = new SqlDatabaseFixture())
            {
                //判断当前人员是否为超级管理员若是超级管理员则具有最大权限
                IList<SystemMenuButtonByViewRote> menuButton = new List<SystemMenuButtonByViewRote>();
                //判断该用户是否为超级管理员:若是超级管理员则显示所有模块
                var userInfo = EipHttpContext.CurrentUser();
                //如果是超级管理员
                if (userInfo.IsAdmin)
                {
                    var data = (await fixture.Db.SystemMenuButtons.FindAllAsync<SystemMenuButtonByViewRote>(f =>
                      f.MenuId == viewRote.MenuId,
                    q => q.Buttons)).ToList();
                    if (data.Any())
                    {
                        return OperateStatus<IEnumerable<SystemMenuButtonByViewRote>>.Success(data.First().Buttons.Where(f => !f.IsFreeze).OrderBy(o => o.OrderNo));
                    }
                    return OperateStatus<IEnumerable<SystemMenuButtonByViewRote>>.Success(menuButton);
                }
                return OperateStatus<IEnumerable<SystemMenuButtonByViewRote>>.Success((await _menuButtonRepository.FindMenuButton(viewRote)).ToList());
            }
        }
        /// <summary>
        /// 获取登录人员对应模块下的功能项
        /// </summary>
        /// <param name="viewRote">路由信息</param>
        /// <returns></returns>
        public async Task<OperateStatus<IEnumerable<SystemMobileMenuButtonByViewRote>>> FindMobileMenuButton(ViewRote viewRote)
        {
            using (var fixture = new SqlDatabaseFixture())
            {
                //判断当前人员是否为超级管理员若是超级管理员则具有最大权限
                IList<SystemMobileMenuButtonByViewRote> menuButton = new List<SystemMobileMenuButtonByViewRote>();
                //判断该用户是否为超级管理员:若是超级管理员则显示所有模块
                var userInfo = EipHttpContext.CurrentUser();
                //如果是超级管理员
                if (userInfo.IsAdmin)
                {
                    var data = (await fixture.Db.SystemMobileMenuButtons.FindAllAsync<SystemMobileMenuButtonByViewRote>(f =>
                      f.MobileMenuId == viewRote.MenuId,
                    q => q.Buttons)).ToList();
                    if (data.Any())
                    {
                        return OperateStatus<IEnumerable<SystemMobileMenuButtonByViewRote>>.Success(data.First().Buttons.Where(f => !f.IsFreeze).OrderBy(o => o.OrderNo));
                    }
                    return OperateStatus<IEnumerable<SystemMobileMenuButtonByViewRote>>.Success(menuButton);
                }
                return OperateStatus<IEnumerable<SystemMobileMenuButtonByViewRote>>.Success((await _menuButtonRepository.FindMobileMenuButton(viewRote)).ToList());
            }
        }

        /// <summary>
        /// 获取登录人员对应模块下的功能项
        /// </summary>
        /// <param name="mvcRote">路由信息</param>
        /// <param name="userId">用户Id</param>
        /// <returns></returns>
        public async Task<OperateStatus<IEnumerable<SystemMobileMenuButtonFindOutput>>> FindMobileMenuButtonByMenuIdAndUserId(SystemMobileMenuButtonFindInput input)
        {
            using (var fixture = new SqlDatabaseFixture())
            {
                //判断该用户是否为超级管理员:若是超级管理员则显示所有模块
                var userInfo = EipHttpContext.CurrentUser();
                //如果是超级管理员
                if (userInfo.IsAdmin)
                {
                    var data = (await fixture.Db.SystemMobileMenuButton.FindAllAsync(f => f.MobileMenuId == input.MobileMenuId && f.IsFreeze == false)).ToList();
                    return OperateStatus<IEnumerable<SystemMobileMenuButtonFindOutput>>.Success(data.MapTo<SystemMobileMenuButton, SystemMobileMenuButtonFindOutput>());
                }
                return OperateStatus<IEnumerable<SystemMobileMenuButtonFindOutput>>.Success((await _systemMobileMenuButtonRepository.FindMobileMenuButtonByMenuIdAndUserId(input)).ToList());
            }
        }

        /// <summary>
        /// 获取角色，组等具有的权限
        /// </summary>
        /// <param name="privilegeMaster">类型:角色，人员，组等</param>
        /// <param name="privilegeMasterValue">对应值</param>
        /// <returns></returns>
        public async Task<OperateStatus<IEnumerable<SystemPermission>>> FindSystemPermissionsByPrivilegeMasterValueAndValue(EnumPrivilegeMaster privilegeMaster,
            Guid? privilegeMasterValue = null)
        {
            return OperateStatus<IEnumerable<SystemPermission>>.Success(privilegeMasterValue != null ? await FindAllAsync(f => f.PrivilegeMasterValue == privilegeMasterValue && f.PrivilegeMaster == privilegeMaster.ToShort()) : await FindAllAsync(f => f.PrivilegeMaster == privilegeMaster.ToShort()));
        }

        #region 数据权限
        /// <summary>
        /// 获取数据权限Sql
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<OperateStatus<string>> FindDataPermissionSql(ViewRote input)
        {
            StringBuilder stringBuilder = new StringBuilder();
            //获取当前用户是否为超级管理员
            var userInfo = EipHttpContext.CurrentUser();
            if (userInfo.IsAdmin)
            {
                return OperateStatus<string>.Success("  1=1");
            }
            IList<SystemData> datas = (await _permissionRepository.FindDataPermission(input)).ToList();
            if (datas.Any())
            {
                foreach (var data in datas)
                {
                    if (!data.RuleSql.IsNullOrEmpty())
                    {
                        //替换Html标签
                        data.RuleSql = data.RuleSql.ReplaceHtmlTag();
                        //是否具有规则数据
                        if (!data.RuleJson.IsNullOrEmpty())
                        {
                            IList<SystemDataRuleJsonDoubleWay> ruleJsons = JsonConvert.DeserializeObject<IList<SystemDataRuleJsonDoubleWay>>(data.RuleJson).ToList();
                            foreach (var ruleJson in ruleJsons)
                            {
                                //替换Sql
                                data.RuleSql = data.RuleSql.Replace(ruleJson.Field, ruleJson.Value.InSql());
                            }
                        }
                        //替换固定信息
                        data.RuleSql = (await GetRuleSql(data.RuleSql, input.UserId)).Data;
                        //追加替换后的Sql
                        stringBuilder.Append(data.RuleSql + " OR ");
                    }
                }
            }
            //去除最后一个OR
            string sql = stringBuilder.ToString();
            sql = sql.Contains("OR") ? sql.Substring(0, sql.Length - 3) : sql;
            sql = sql.Trim().IsNullOrEmpty() ? "1<>1" : "(" + sql + ")";
            return OperateStatus<string>.Success(sql);
        }

        /// <summary>
        /// 替换规则Sql
        /// </summary>
        /// <param name="ruleSql"></param>
        /// <param name="userId">用户Id</param>
        /// <returns></returns>
        private async Task<OperateStatus<string>> GetRuleSql(string ruleSql, Guid userId)
        {
            using (var fixture = new SqlDatabaseFixture())
            {
                //获取角色、组、岗位数据
                var privilegeMaster = new List<short>
                {
                    EnumPrivilegeMaster.角色.ToShort(),
                    EnumPrivilegeMaster.组.ToShort(),
                    EnumPrivilegeMaster.岗位.ToShort(),
                    EnumPrivilegeMaster.组织架构.ToShort()
                };
                var privilegeDetailDtos = (await fixture.Db.SystemPermissionUser.SetSelect(s => new { s.PrivilegeMaster, s.PrivilegeMasterValue }).FindAllAsync(f => f.PrivilegeMasterUserId == userId && privilegeMaster.Contains(f.PrivilegeMaster))).ToList();

                if (ruleSql.Contains("{所有}"))
                {
                    ruleSql = ruleSql.Replace("{所有}", "1=1");
                }
                if (ruleSql.Contains("{当前用户}"))
                {
                    ruleSql = ruleSql.Replace("{当前用户}", userId.ToString());
                }
                if (ruleSql.Contains("{所在组织}"))
                {
                    //获取当前人员所在组织
                    ruleSql = ruleSql.Replace("{所在组织}", privilegeDetailDtos.Where(w => w.PrivilegeMaster == EnumPrivilegeMaster.组织架构.GetHashCode()).Select(d => d.PrivilegeMasterValue).ToList().ExpandAndToString().InSql());
                }
                if (ruleSql.Contains("{所在组织及下级组织}"))
                {
                    //查找机构
                    var orgId = privilegeDetailDtos.Where(w => w.PrivilegeMaster == EnumPrivilegeMaster.组织架构.GetHashCode()).Select(d => d.PrivilegeMasterValue);
                    var organizationId = new List<Guid>();
                    //获取当前人员所在组织及下级组织
                    foreach (var item in orgId)
                    {
                        var orgs = await fixture.Db.SystemOrganization.SetSelect(s => new { s.OrganizationId }).FindAllAsync(f => f.ParentIds.Contains(item.ToString()));
                        foreach (var o in orgs.ToList())
                        {
                            organizationId.Add(o.OrganizationId);
                        }
                    }
                    var parentIds = orgId.FirstOrDefault().ToString();
                    ruleSql = ruleSql.Replace("{所在组织及下级组织}", organizationId.Distinct().ExpandAndToString().InSql());
                }
                if (ruleSql.Contains("{所在组织代码}"))
                {
                    //获取当前人员所在组织
                    ruleSql = ruleSql.Replace("{所在组织代码}", privilegeDetailDtos.Where(w => w.PrivilegeMaster == EnumPrivilegeMaster.组织架构.GetHashCode()).Select(d => d.PrivilegeMasterValue).ToList().ExpandAndToString().InSql());
                }
                if (ruleSql.Contains("{所在岗位}"))
                {
                    //获取当前人员所在岗位
                    ruleSql = ruleSql.Replace("{所在岗位}", privilegeDetailDtos.Where(w => w.PrivilegeMaster == EnumPrivilegeMaster.岗位.GetHashCode()).Select(d => d.PrivilegeMasterValue).ToList().ExpandAndToString().InSql());
                }
                if (ruleSql.Contains("{所在工作组}"))
                {
                    //获取当前人员所在工作组
                    ruleSql = ruleSql.Replace("{所在工作组}", privilegeDetailDtos.Where(w => w.PrivilegeMaster == EnumPrivilegeMaster.组.GetHashCode()).Select(d => d.PrivilegeMasterValue).ToList().ExpandAndToString().InSql());
                }
                return OperateStatus<string>.Success(ruleSql);
            }
        }
        #endregion

        #endregion
    }
}