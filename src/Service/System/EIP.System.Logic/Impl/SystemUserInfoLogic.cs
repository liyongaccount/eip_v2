/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Base.Models.Entities.System;
using EIP.Base.Models.Resx;
using EIP.Base.Repository.Fixture;
using EIP.Common.Core.Context;
using EIP.Common.Extension;
using EIP.Common.Language.Resource;
using EIP.Common.Logic;
using EIP.Common.Models;
using EIP.Common.Models.Dtos;
using EIP.Common.Models.Dtos.Reports;
using EIP.Common.Models.Paging;
using EIP.Common.Models.Tree;
using EIP.Common.Util;
using EIP.System.Models.Dtos.Login;
using EIP.System.Models.Dtos.Permission;
using EIP.System.Models.Dtos.User;
using EIP.System.Models.Enums;
using EIP.System.Repository;
using EIP.System.Repository.Impl;
using Lazy.Captcha.Core;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace EIP.System.Logic.Impl
{
    /// <summary>
    /// 用户业务逻辑实现
    /// </summary>
    public class SystemUserInfoLogic : DapperAsyncLogic<SystemUserInfo>, ISystemUserInfoLogic
    {
        #region 构造函数
        private readonly ICaptcha _captcha;
        private readonly ISystemPermissionUserLogic _permissionUserLogic;
        private readonly ISystemUserInfoRepository _userInfoRepository;
        private readonly IOptions<EIPConfig> _configOptions;

        /// <summary>
        /// 系统人员
        /// </summary>
        /// <param name="userInfoRepository"></param>
        /// <param name="permissionUserLogic"></param>
        /// <param name="configOptions"></param>
        /// <param name="organizationLogic"></param>
        public SystemUserInfoLogic(ISystemUserInfoRepository userInfoRepository,
            ISystemPermissionUserLogic permissionUserLogic,
            IOptions<EIPConfig> configOptions,
            ICaptcha captcha)
        {
            _userInfoRepository = userInfoRepository;
            _permissionUserLogic = permissionUserLogic;
            _configOptions = configOptions;
            _captcha = captcha;
        }

        /// <summary>
        /// 系统用户
        /// </summary>
        /// <param name="configOptions"></param>
        /// <param name="organizationLogic"></param>
        public SystemUserInfoLogic(IOptions<EIPConfig> configOptions, ISystemOrganizationLogic organizationLogic)
        {
            _configOptions = configOptions;
            _userInfoRepository = new SystemUserInfoRepository();
        }

        #endregion

        #region 方法

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="input">分页参数</param>
        /// <returns></returns>
        public async Task<OperateStatus<PagedResults<SystemUserOutput>>> Find(SystemUserPagingInput input)
        {
            if (input.DataSql.IsNullOrEmpty()) return OperateStatus<PagedResults<SystemUserOutput>>.Success(new PagedResults<SystemUserOutput>());
            var datas = await _userInfoRepository.Find(input);
            foreach (var data in datas.Data)
            {
                if (data.HeadImage.IsNotNullOrEmpty())
                {
                    data.HeadImage = ConfigurationUtil.GetFileServer() + data.HeadImage;
                }
            }
            return OperateStatus<PagedResults<SystemUserOutput>>.Success(datas);
        }

        /// <summary>
        /// 用户公共信息
        /// </summary>
        /// <param name="input">分页参数</param>
        /// <returns></returns>
        public async Task<OperateStatus<IEnumerable<SystemUserFindCommonOutput>>> FindCommon(SystemUserFindCommonInput input)
        {
            return OperateStatus<IEnumerable<SystemUserFindCommonOutput>>.Success(await _userInfoRepository.FindCommon(input));
        }

        /// <summary>
        /// 检测配置项代码是否已经具有重复项
        /// </summary>
        /// <param name="input">需要验证的参数</param>
        /// <returns></returns>
        public async Task<OperateStatus> CheckCode(SystemUserCheckUserCodeInput input)
        {
            var operateStatus = new OperateStatus();
            var count = !input.Id.IsEmptyGuid() ?
                await CountAsync(c => c.Code == input.Code && c.UserId != input.Id) :
                await CountAsync(c => c.Code == input.Code);
            if (count != 0)
            {
                operateStatus.Code = ResultCode.Error;
                operateStatus.Msg = string.Format(Chs.HaveCode, input.Code);
            }
            else
            {
                operateStatus.Code = ResultCode.Success;
                operateStatus.Msg = Chs.CheckSuccessful;
            }
            return operateStatus;
        }

        /// <summary>
        /// 保存人员信息
        /// </summary>
        /// <param name="input">人员信息</param>
        /// <returns></returns>
        public async Task<OperateStatus> Save(SystemUserSaveInput input)
        {
            OperateStatus operateStatus;
            var user = await FindAsync(f => f.UserId == input.UserId);
            var currentUser = EipHttpContext.CurrentUser();
            var organizationId = new List<Guid>();
            if (input.UserOrganizationIds.IsNotNullOrEmpty())
            {
                organizationId.AddRange(input.UserOrganizationIds.Split(',').Select(s => Guid.Parse(s)));
            }
            List<string> userOrganizationNames = new List<string>();
            if (organizationId.Any())
            {
                using (var fix = new SqlDatabaseFixture())
                {
                    userOrganizationNames = (await fix.Db.SystemOrganization.SetSelect(s => new { s.Name }).FindAllAsync(f => organizationId.Contains(f.OrganizationId))).Select(s => s.Name).ToList();
                }
            }

            if (user == null)
            {
                //新增
                if (!input.Code.IsNullOrEmpty())
                {
                    input.Password = DEncryptUtil.Encrypt("123456", _configOptions.Value.PasswordKey);
                }
                SystemUserInfo userInfoMap = input.MapTo<SystemUserInfo>();
                userInfoMap.UserOrganizationIds = organizationId.ExpandAndToString();
                userInfoMap.UserOrganizationNames = userOrganizationNames.ExpandAndToString();

                userInfoMap.CreateTime = DateTime.Now;
                userInfoMap.CreateUserId = currentUser.UserId;
                userInfoMap.CreateUserName = currentUser.Name;
                userInfoMap.UpdateTime = DateTime.Now;
                userInfoMap.UpdateUserId = currentUser.UserId;
                userInfoMap.UpdateUserName = currentUser.Name;

                operateStatus = await InsertAsync(userInfoMap);
                if (operateStatus.Code == ResultCode.Success)
                {
                    using (var fix = new SqlDatabaseFixture())
                    {
                        var privilegeMaster = EnumPrivilegeMaster.组织架构;
                        await fix.Db.SystemPermissionUser.DeleteAsync(d => d.PrivilegeMaster == privilegeMaster.ToShort() && d.PrivilegeMasterUserId == input.UserId);
                        IList<SystemPermissionUser> systemPermissionUsers =
                            organizationId.Select(roleId => new SystemPermissionUser
                            {
                                PrivilegeMaster = (byte)privilegeMaster,
                                PrivilegeMasterUserId = input.UserId,
                                PrivilegeMasterValue = roleId,
                                IsRelationOrganization = true,
                            }).ToList();
                        if (systemPermissionUsers.Any())
                        {
                            //批量保存
                            await fix.Db.SystemPermissionUser.BulkInsertAsync(systemPermissionUsers);
                        }

                        SystemPermissionUser systemPermissionUser = new SystemPermissionUser
                        {
                            PrivilegeMaster = (byte)privilegeMaster,
                            PrivilegeMasterUserId = input.UserId,
                            PrivilegeMasterValue = input.OrganizationId,
                            IsRelationOrganization = false,
                        };
                        await fix.Db.SystemPermissionUser.InsertAsync(systemPermissionUser);
                    }
                    operateStatus = await _permissionUserLogic.SavePermissionMasterValueBeforeDelete(EnumPrivilegeMaster.人员, input.UserId, new List<Guid> { input.UserId });
                    if (input.RoleId.IsNotNullOrEmpty())
                    {
                        await _permissionUserLogic.SavePermissionMasterValueBeforeDelete(EnumPrivilegeMaster.角色, input.UserId, input.RoleId.Split(',').Select(s => Guid.Parse(s)).ToList());
                    }
                    if (operateStatus.Code == ResultCode.Success)
                    {
                        return operateStatus;
                    }
                }
                else
                {
                    return operateStatus;
                }
            }
            else
            {
                input.Password = user.Password;
                SystemUserInfo userInfoMap = input.MapTo<SystemUserInfo>();
                userInfoMap.UserOrganizationIds = organizationId.ExpandAndToString();
                userInfoMap.UserOrganizationNames = userOrganizationNames.ExpandAndToString();
                userInfoMap.Id = user.Id;

                userInfoMap.CreateTime = user.CreateTime;
                userInfoMap.CreateUserId = user.CreateUserId;
                userInfoMap.CreateUserName = user.CreateUserName;

                userInfoMap.UpdateTime = DateTime.Now;
                userInfoMap.UpdateUserId = currentUser.UserId;
                userInfoMap.UpdateUserName = currentUser.Name;

                operateStatus = await UpdateAsync(userInfoMap);
                //添加用户到组织机构
                if (operateStatus.Code == ResultCode.Success)
                {
                    using (var fix = new SqlDatabaseFixture())
                    {
                        var privilegeMaster = EnumPrivilegeMaster.组织架构;
                        await fix.Db.SystemPermissionUser.DeleteAsync(d => d.PrivilegeMaster == privilegeMaster.ToShort() && d.PrivilegeMasterUserId == input.UserId);
                        IList<SystemPermissionUser> systemPermissionUsers =
                            organizationId.Select(roleId => new SystemPermissionUser
                            {
                                PrivilegeMaster = (byte)privilegeMaster,
                                PrivilegeMasterUserId = input.UserId,
                                PrivilegeMasterValue = roleId,
                                IsRelationOrganization = true,
                            }).ToList();
                        if (systemPermissionUsers.Any())
                        {
                            //批量保存
                            await fix.Db.SystemPermissionUser.BulkInsertAsync(systemPermissionUsers);
                        }

                        SystemPermissionUser systemPermissionUser = new SystemPermissionUser
                        {
                            PrivilegeMaster = (byte)privilegeMaster,
                            PrivilegeMasterUserId = input.UserId,
                            PrivilegeMasterValue = input.OrganizationId,
                            IsRelationOrganization = false,
                        };
                        await fix.Db.SystemPermissionUser.InsertAsync(systemPermissionUser);
                    }
                    operateStatus = await _permissionUserLogic.SavePermissionMasterValueBeforeDelete(EnumPrivilegeMaster.人员, input.UserId, new List<Guid> { input.UserId });
                    await _permissionUserLogic.DeletePrivilegeMasterUser(input.UserId, EnumPrivilegeMaster.角色);
                    if (input.RoleId.IsNotNullOrEmpty())
                    {
                        await _permissionUserLogic.SavePermissionMasterValueBeforeDelete(EnumPrivilegeMaster.角色, input.UserId, input.RoleId.Split(',').Select(s => Guid.Parse(s)).ToList());
                    }
                }
                return operateStatus;
            }
            return operateStatus;
        }

        /// <summary>
        /// 删除用户信息
        /// </summary>
        /// <param name="input">用户id</param>
        /// <returns></returns>
        public async Task<OperateStatus> Delete(IdInput<string> input)
        {
            OperateStatus operateStatus = new OperateStatus();
            using (var fixture = new SqlDatabaseFixture(false))
            {
                var trans = fixture.Db.BeginTransaction();
                try
                {
                    foreach (var id in input.Id.Split(','))
                    {
                        var userId = Guid.Parse(id);
                        var userInfo = await FindAsync(f => f.UserId == userId);
                        if (userInfo == null)
                        {
                            operateStatus.Code = ResultCode.Error;
                            operateStatus.Msg = ResourceSystem.人员不存在;
                            return operateStatus;
                        }
                        if (!await fixture.Db.SystemUserInfo.DeleteAsync(d => d.UserId == userId, trans)) continue;
                        operateStatus.Msg = Chs.Successful;
                        operateStatus.Code = ResultCode.Success;
                    }

                    foreach (var id in input.Id.Split(','))
                    {
                        var userId = Guid.Parse(id);
                        await _permissionUserLogic.DeletePrivilegeMasterUser(userId);
                    }
                    trans.Commit();
                }
                catch (Exception e)
                {
                    trans.Rollback();
                    operateStatus.Msg = e.Message;
                }
            }
            return operateStatus;
        }

        /// <summary>
        /// 根据用户Id获取该用户信息
        /// </summary>
        /// <param name="input">用户Id</param>
        /// <returns></returns>
        public async Task<OperateStatus<SystemUserDetailOutput>> FindDetailByUserId(IdInput input)
        {
            using (var fixture = new SqlDatabaseFixture())
            {
                //获取用户基本信息
                var userDto = (await fixture.Db.SystemUserInfo.FindAsync(f => f.UserId == input.Id)).MapTo<SystemUserOutput>();
                if (userDto.HeadImage.IsNotNullOrEmpty())
                {
                    userDto.HeadImage = ConfigurationUtil.GetFileServer() + userDto.HeadImage;
                }
                var allOrgs = (await fixture.Db.SystemOrganization.FindAllAsync()).ToList();
                //转换
                var userDetailDto = userDto.MapTo<SystemUserDetailOutput>();
                //获取角色、组、岗位数据
                var privilegeMaster = new List<short>
                {
                    EnumPrivilegeMaster.角色.ToShort(),
                    EnumPrivilegeMaster.组.ToShort(),
                    EnumPrivilegeMaster.岗位.ToShort()
                };
                var systemPermissionUsers = (await fixture.Db.SystemPermissionUser.FindAllAsync(f => f.PrivilegeMasterUserId == input.Id && privilegeMaster.Contains(f.PrivilegeMaster))).ToList();
                //角色集合
                var roleId = systemPermissionUsers.Where(w => w.PrivilegeMaster == EnumPrivilegeMaster.角色.ToShort()).Select(s => s.PrivilegeMasterValue).ToList();
                var roles = (await fixture.Db.SystemRole.FindAllAsync(f => roleId.Contains(f.RoleId))).ToList();

                //组
                var groupId = systemPermissionUsers.Where(w => w.PrivilegeMaster == EnumPrivilegeMaster.组.ToShort()).Select(s => s.PrivilegeMasterValue).ToList();
                var groups = (await fixture.Db.SystemGroup.FindAllAsync(f => groupId.Contains(f.GroupId))).ToList();

                //岗位
                var postId = systemPermissionUsers.Where(w => w.PrivilegeMaster == EnumPrivilegeMaster.岗位.ToShort()).Select(s => s.PrivilegeMasterValue).ToList();
                var posts = (await fixture.Db.SystemPost.FindAllAsync(f => postId.Contains(f.PostId))).ToList();

                IList<SystemPrivilegeDetailListOutput> privilegeDetailDtos = new List<SystemPrivilegeDetailListOutput>();
                foreach (var permissionUser in systemPermissionUsers)
                {
                    if (permissionUser.PrivilegeMaster == EnumPrivilegeMaster.角色.ToShort())
                    {
                        //查询角色
                        var role = roles.FirstOrDefault(w => w.RoleId == permissionUser.PrivilegeMasterValue);
                        if (role == null) continue;
                        privilegeDetailDtos.Add(new SystemPrivilegeDetailListOutput
                        {
                            Name = role.Name,
                            OrganizationId = role.OrganizationId,
                            PrivilegeMaster = EnumPrivilegeMaster.角色,
                            PrivilegeMasterValue = permissionUser.PrivilegeMasterValue
                        });
                    }
                    else if (permissionUser.PrivilegeMaster == EnumPrivilegeMaster.组.ToShort())
                    {
                        var group = groups.FirstOrDefault(w => w.GroupId == permissionUser.PrivilegeMasterValue);
                        if (group == null) continue;
                        privilegeDetailDtos.Add(new SystemPrivilegeDetailListOutput
                        {
                            Name = group.Name,
                            OrganizationId = group.OrganizationId,
                            PrivilegeMaster = EnumPrivilegeMaster.组,
                            PrivilegeMasterValue = permissionUser.PrivilegeMasterValue
                        });
                    }
                    else if (permissionUser.PrivilegeMaster == EnumPrivilegeMaster.岗位.ToShort())
                    {
                        var post = posts.FirstOrDefault(w => w.PostId == permissionUser.PrivilegeMasterValue);
                        if (post == null) continue;
                        privilegeDetailDtos.Add(new SystemPrivilegeDetailListOutput
                        {
                            Name = post.Name,
                            OrganizationId = post.OrganizationId,
                            PrivilegeMaster = EnumPrivilegeMaster.岗位,
                            PrivilegeMasterValue = permissionUser.PrivilegeMasterValue
                        });
                    }
                }

                //便利
                foreach (var dto in privilegeDetailDtos)
                {
                    string description = string.Empty;
                    var organization = allOrgs.FirstOrDefault(w => w.OrganizationId == dto.OrganizationId);
                    if (organization != null && !organization.ParentIds.IsNullOrEmpty())
                    {
                        foreach (var parent in organization.ParentIds.Split(','))
                        {
                            //查找上级
                            var dicinfo = allOrgs.FirstOrDefault(w => w.OrganizationId.ToString() == parent);
                            if (dicinfo != null) description += dicinfo.Name + "/";
                        }

                        if (!description.IsNullOrEmpty())
                            description = description.TrimEnd('/');
                    }
                    dto.Organization = description;
                }

                //角色
                userDetailDto.Role = privilegeDetailDtos.Where(w => w.PrivilegeMaster == EnumPrivilegeMaster.角色).ToList();
                //组
                userDetailDto.Group = privilegeDetailDtos.Where(w => w.PrivilegeMaster == EnumPrivilegeMaster.组).ToList();
                //岗位
                userDetailDto.Post = privilegeDetailDtos.Where(w => w.PrivilegeMaster == EnumPrivilegeMaster.岗位).ToList();
                return OperateStatus<SystemUserDetailOutput>.Success(userDetailDto);
            }
        }

        /// <summary>
        /// 根据用户Id重置某人密码
        /// </summary>
        /// <param name="input">用户Id</param>
        /// <returns></returns>
        public async Task<OperateStatus> ResetPassword(SystemUserResetPasswordInput input)
        {
            var operateStatus = new OperateStatus();
            //将传入的密码加密
            var encryptPwd = DEncryptUtil.Encrypt(input.EncryptPassword, _configOptions.Value.PasswordKey);
            //获取人员信息
            var user = await FindAsync(f => f.UserId == input.Id);
            if (user == null)
            {
                operateStatus.Msg = ResourceSystem.人员不存在;
                return operateStatus;
            }
            user.Password = encryptPwd;
            operateStatus = await UpdateAsync(user);
            if (operateStatus.Code == ResultCode.Success)
            {
                operateStatus.Msg = string.Format(ResourceSystem.重置密码成功, input.EncryptPassword);
            }
            return operateStatus;
        }

        /// <summary>
        /// 保存用户头像
        /// </summary>
        /// <param name="input">用户头像</param>
        /// <returns></returns>
        public async Task<OperateStatus<string>> SaveHeadImage(SystemUserSaveHeadImageInput input)
        {
            var operateStatus = new OperateStatus<string>();
            string hearderpath = $"\\userheader\\{input.UserId}.png";
            string path = ConfigurationUtil.GetUploadPath() + hearderpath;
            //转换并保存图片
            input.HeadImage.ConvertBase64ToImage(path);
            //获取人员信息
            var user = await FindAsync(f => f.UserId == input.UserId);
            if (user == null)
            {
                operateStatus.Msg = ResourceSystem.人员不存在;
                return operateStatus;
            }
            user.HeadImage = hearderpath;
            await UpdateAsync(user);
            operateStatus.Code = ResultCode.Success;
            operateStatus.Msg = Chs.Successful;
            operateStatus.Data = ConfigurationUtil.GetFileServer() + hearderpath;
            return operateStatus;
        }

        /// <summary>
        /// 保存修改后密码信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<OperateStatus> SaveChangePassword(SystemUserChangePasswordInput input)
        {
            var operateStatus = new OperateStatus();
            //后台再次验证是否一致
            if (!input.NewPassword.Equals(input.ConfirmNewPassword))
            {
                operateStatus.Msg = "录入的新密码和确认密码不一致。";
                return operateStatus;
            }
            //旧密码是否正确
            operateStatus = await CheckOldPassword(new CheckSameValueInput { Id = input.UserId, Param = input.OldPassword });
            if (operateStatus.Code == ResultCode.Error)
            {
                return operateStatus;
            }
            //将传入的密码加密
            var encryptPwd = DEncryptUtil.Encrypt(input.NewPassword, _configOptions.Value.PasswordKey);
            //获取人员信息
            var user = await FindAsync(f => f.UserId == input.UserId);
            if (user == null)
            {
                operateStatus.Msg = ResourceSystem.人员不存在;
                return operateStatus;
            }
            user.Password = encryptPwd;
            operateStatus = await UpdateAsync(user);
            if (operateStatus.Code == ResultCode.Success)
            {
                operateStatus.Msg = string.Format(ResourceSystem.重置密码成功, input.NewPassword);
            }
            return operateStatus;
        }

        /// <summary>
        /// 验证旧密码是否输入正确
        /// </summary>
        /// <param name="input">需要验证的参数</param>
        /// <returns></returns>
        public async Task<OperateStatus> CheckOldPassword(CheckSameValueInput input)
        {
            var operateStatus = new OperateStatus();
            input.Param = DEncryptUtil.Encrypt(input.Param, _configOptions.Value.PasswordKey);
            if ((await CountAsync(c => c.Password == input.Param && c.UserId == input.Id)) == 0)
            {
                operateStatus.Code = ResultCode.Error;
                operateStatus.Msg = ResourceSystem.旧密码不正确;
            }
            else
            {
                operateStatus.Code = ResultCode.Success;
            }
            return operateStatus;
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="input">分页参数</param>
        /// <returns></returns>
        public async Task<OperateStatus<IList<SystemUserFindOrganizationUserOutput>>> FindOrganizationUser()
        {
            IList<SystemUserFindOrganizationUserOutput> organizationUserOutputs = new List<SystemUserFindOrganizationUserOutput>();
            using (var fix = new SqlDatabaseFixture())
            {
                var orgs = (await fix.Db.SystemOrganization.FindAllAsync()).OrderBy(o => o.OrderNo);

                var users = (await _userInfoRepository.Find(new SystemUserPagingInput
                {
                    PrivilegeMaster = EnumPrivilegeMaster.组织架构,
                    Size = int.MaxValue
                })).Data.ToList();
                foreach (var item in orgs)
                {
                    SystemUserFindOrganizationUserOutput organizationUserOutput = new SystemUserFindOrganizationUserOutput
                    {
                        OrganizationId = item.OrganizationId,
                        Name = item.Name,
                        ParentId = item.ParentId,
                        ParentIdsName = item.ParentIdsName,
                        ParentIds = item.ParentIds,
                        ParentName = item.ParentName,
                        ShortName = item.ShortName
                    };
                    var user = users.Where(w => w.OrganizationId == item.OrganizationId);
                    foreach (var u in user)
                    {
                        if (u.HeadImage.IsNotNullOrEmpty())
                        {
                            u.HeadImage = ConfigurationUtil.GetFileServer() + u.HeadImage;
                        }
                        organizationUserOutput.Users.Add(new FindOrganizationUserOutput
                        {
                            UserId = u.UserId,
                            OrganizationId = u.OrganizationId,
                            Code = u.Code,
                            Name = u.Name,
                            Mobile = u.Mobile,
                            Email = u.Email,
                            OtherContactInformation = u.OtherContactInformation,
                            Remark = u.Remark,
                            HeadImage = u.HeadImage
                        });
                    }
                    organizationUserOutputs.Add(organizationUserOutput);
                }
            }
            return OperateStatus<IList<SystemUserFindOrganizationUserOutput>>.Success(organizationUserOutputs);
        }

        /// <summary>
        /// 获取组织机构人员树
        /// </summary>
        /// <returns></returns>
        public async Task<OperateStatus<IEnumerable<JsTreeEntity>>> FindOrganizationUserTree()
        {
            using (var fix = new SqlDatabaseFixture())
            {
                var orgs = await fix.Db.SystemOrganization.FindAllAsync(f => f.IsFreeze == false);
                var users = (await FindAllAsync(f => f.IsFreeze == false)).ToList();
                IList<JsTreeEntity> treeEntities = new List<JsTreeEntity>();
                foreach (var org in orgs)
                {
                    treeEntities.Add(new JsTreeEntity
                    {
                        id = org.OrganizationId,
                        parent = org.ParentId == Guid.Empty ? "#" : org.ParentId.ToString(),
                        text = org.Name,
                        icon = "fa fa-cubes"
                    });
                    //查找人员
                    var user = users.Where(w => w.OrganizationId == org.OrganizationId);
                    foreach (var item in user)
                    {
                        treeEntities.Add(new JsTreeEntity
                        {
                            id = item.UserId,
                            parent = org.OrganizationId,
                            text = item.Name + $"({item.Code})",
                            icon = "fa fa-user"
                        });
                    }
                }
                return OperateStatus<IEnumerable<JsTreeEntity>>.Success(treeEntities);
            }


        }

        /// <summary>
        /// 根据用户Id获取
        /// </summary>
        /// <param name="input">用户Id</param>
        /// <returns></returns>
        public async Task<OperateStatus<SystemUserFindHeadByIdOutput>> FindById(IdInput input)
        {
            var user = (await FindAsync(f => f.UserId == input.Id)).MapTo<SystemUserFindHeadByIdOutput>();
            if (user.HeadImage.IsNotNullOrEmpty())
            {
                user.HeadImageFormat = ConfigurationUtil.GetFileServer() + user.HeadImage;
            }
            return OperateStatus<SystemUserFindHeadByIdOutput>.Success(user);
        }

        /// <summary>
        /// 冻结
        /// </summary>
        /// <returns></returns>
        public async Task<OperateStatus> IsFreeze(IdInput input)
        {
            var data = await FindAsync(f => f.UserId == input.Id);
            data.IsFreeze = !data.IsFreeze;
            return await UpdateAsync(data);
        }
        #endregion

        #region 登录

        /// <summary>
        /// 生成验证码
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public OperateStatus<MemoryStream> Captcha()
        {
            try
            {
                var info = _captcha.Generate("EIP_Captcha");
                // 有多处验证码且过期时间不一样，可传第二个参数覆盖默认配置。
                //var info = _captcha.Generate(id,120);
                var stream = new MemoryStream(info.Bytes);
                return OperateStatus<MemoryStream>.Success(stream);
            }
            catch (Exception)
            {
                return OperateStatus<MemoryStream>.Error("生成异常");
            }
        }

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<OperateStatus<SystemLoginOutput>> Login(SystemLoginInput input)
        {
            try
            {
                //解密
                RSACryptoService loginRsa = new RSACryptoService(ResourceKey.后台加密私钥, ResourceKey.后台加密公钥);
                input.Code = loginRsa.Decrypt(input.Code);
                input.Password = loginRsa.Decrypt(input.Password);
                input.Captcha = loginRsa.Decrypt(input.Captcha);
            }
            catch (Exception ex)
            {
                return OperateStatus<SystemLoginOutput>.Error("用户名或密码错误");
            }
            //查看验证码是否正确
            var checkCaptcha = _captcha.Validate("EIP_Captcha", input.Captcha);
            if (!checkCaptcha)
            {
                return OperateStatus<SystemLoginOutput>.Error("验证码错误");
            }
            //将传入的密码加密
            var encryptPwd = DEncryptUtil.Encrypt(input.Password, ConfigurationUtil.GetSection("EIP:PasswordKey"));
            //查询信息
            input.Password = encryptPwd;

            var data = await _userInfoRepository.Login(input);
            //是否存在
            if (data == null)
            {
                return OperateStatus<SystemLoginOutput>.Error("用户名或密码错误");
            }
            //是否冻结
            if (data.IsFreeze)
            {
                return OperateStatus<SystemLoginOutput>.Error("登录用户已冻结");
            }
            data.HeadImage = data.HeadImage.IsNotNullOrEmpty() ? ConfigurationUtil.GetFileServer() + data.HeadImage : data.HeadImage;
            //更新用户最后一次登录时间
            await _userInfoRepository.UpdateTime(data.UserId);
            data.LoginId = CombUtil.NewComb();
            return OperateStatus<SystemLoginOutput>.Success(data);
        }
        #endregion

        #region 注册
        /// <summary>
        /// 导入用户
        /// </summary>
        /// <param name="input">集合信息</param>
        /// <returns></returns>
        public async Task<OperateStatus> Register(SystemUserRegisterInput input)
        {
            var operateStatus = new OperateStatus();
            //解密密码
            RSACryptoService registerRsa = new RSACryptoService(ResourceKey.小程序加密私钥, ResourceKey.小程序加密公钥);
            try
            {
                input.Password = registerRsa.Decrypt(input.Password);
                input.OpenId = registerRsa.Decrypt(input.OpenId);
            }
            catch (Exception ex)
            {
                operateStatus.Msg = "提交数据异常,请稍后重试";
                return operateStatus;
            }

            using (var fix = new SqlDatabaseFixture())
            {
                //判断是否已存在用户帐号
                var have = await fix.Db.SystemUserInfo.FindAsync(f => f.Mobile == input.Mobile || f.Code == input.Mobile);
                if (have != null)
                {
                    operateStatus.Msg = "电话号码已存在";
                    return operateStatus;
                }
                var encryptPwd = DEncryptUtil.Encrypt(input.Password, _configOptions.Value.PasswordKey);
                //添加人员及角色
                SystemUserInfo userInfoMap = new SystemUserInfo()
                {
                    UserId = CombUtil.NewComb(),
                    OrganizationId = input.OrganizationId,
                    OrganizationName = input.OrganizationName,
                    Mobile = input.Mobile,
                    Code = input.Mobile,
                    Password = encryptPwd,
                    Name = input.Name,
                    FirstVisitTime = DateTime.Now
                };

                operateStatus = await InsertAsync(userInfoMap);
                if (operateStatus.Code == ResultCode.Success)
                {
                    IList<Guid> organizationId = new List<Guid>();
                    organizationId.Add(input.OrganizationId);
                    operateStatus = await _permissionUserLogic.SavePermissionMasterValueBeforeDelete(EnumPrivilegeMaster.组织架构, userInfoMap.UserId, organizationId);
                    operateStatus = await _permissionUserLogic.SavePermissionMasterValueBeforeDelete(EnumPrivilegeMaster.人员, userInfoMap.UserId, new List<Guid> { userInfoMap.UserId });
                    var roleId = "515f231d-af2b-4acc-8783-315d201cc23f";
                    await _permissionUserLogic.SavePermissionMasterValueBeforeDelete(EnumPrivilegeMaster.角色, userInfoMap.UserId, roleId.Split(',').Select(s => Guid.Parse(s)).ToList());

                    //写入微信人员表
                    var weChatUser = await fix.Db.WeChatUser.FindAsync(f => f.OpenId == input.OpenId);
                    if (weChatUser != null)
                    {
                        weChatUser.UserId = userInfoMap.UserId;
                        await fix.Db.WeChatUser.UpdateAsync(weChatUser);
                    }

                    if (operateStatus.Code == ResultCode.Success)
                    {
                        operateStatus.Msg = "注册成功";
                        return operateStatus;
                    }
                }
                else
                {
                    return operateStatus;
                }
            }
            return operateStatus;
        }
        #endregion


        /// <summary>
        /// 上传头像
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<OperateStatus<List<Guid>>> UploadHeadImage(SystemUserSaveInput input)
        {
            OperateStatus<List<Guid>> operateStatus = new OperateStatus<List<Guid>>();
            try
            {
                var files = input.Files;
                if (files.Any())
                {
                    var uploadPath = ConfigurationUtil.GetUploadPath();
                    if (!Directory.Exists(uploadPath + $"\\userheader\\"))
                    {
                        Directory.CreateDirectory(uploadPath + $"\\userheader\\");
                    }
                    foreach (var file in files)
                    {
                        string path = $"\\userheader\\{input.UserId}.png";
                        string filename = uploadPath + path;
                        using (FileStream fs = File.Create(filename))
                        {
                            file.CopyTo(fs);
                            fs.Flush();
                        }
                        //更新用户信息
                        var user = await FindByIdAsync(input.UserId);
                        user.HeadImage = path;
                        user.UpdateTime = DateTime.Now;
                        await UpdateAsync(user);
                    }
                    operateStatus.Code = ResultCode.Success;
                    operateStatus.Msg = Chs.Successful;
                }
                else
                {
                    operateStatus.Code = ResultCode.Success;
                    operateStatus.Msg = Chs.Successful;
                }
            }
            catch (Exception ex)
            {
                operateStatus.Msg = ex.Message;
            }
            return operateStatus;
        }
        /// <summary>
        /// Excel导出方式
        /// </summary>
        /// <param name="paging">查询参数</param>
        /// <param name="excelReportDto"></param>
        /// <returns></returns>
        public async Task<OperateStatus> ReportExcelUserQuery(SystemUserPagingInput paging,
            ExcelReportDto excelReportDto)
        {
            var operateStatus = new OperateStatus();
            try
            {
                //组装数据
                IList<SystemUserOutput> dtos = (await _userInfoRepository.Find(paging)).Data.ToList();
                var tables = new Dictionary<string, DataTable>(StringComparer.OrdinalIgnoreCase);
                //组装需要导出数据

                operateStatus.Code = ResultCode.Success;
            }
            catch (Exception)
            {
                operateStatus.Code = ResultCode.Error;
            }
            return operateStatus;
        }


        /// <summary>
        /// 导入用户
        /// </summary>
        /// <param name="users"></param>
        /// <returns></returns>
        public async Task<OperateStatus<List<string>>> ImportUser(IList<SystemUserImportDto> users)
        {
            OperateStatus<List<string>> operateStatus = new OperateStatus<List<string>>();
            if (users.Count == 0)
            {
                operateStatus.Msg = "导入数据为空";
                return operateStatus;
            }
            var codeNull = users.Count(c => c.Code.IsNullOrEmpty());
            if (codeNull > 0)
            {
                operateStatus.Msg = "请确认账号均已填写";
                return operateStatus;
            }
            var nameNull = users.Count(c => c.Name.IsNullOrEmpty());
            if (nameNull > 0)
            {
                operateStatus.Msg = "请确认用户真实姓名均已填写";
                return operateStatus;
            }
            var orgNull = users.Count(c => c.ParentIdsName.IsNullOrEmpty());
            if (orgNull > 0)
            {
                operateStatus.Msg = "请确认组织机构名称均已填写";
                return operateStatus;
            }
            //数据检查
            var currentUser = EipHttpContext.CurrentUser();
            List<SystemUserInfo> systemUserSaveInputs = new List<SystemUserInfo>();
            List<SystemUserImportUserRole> userRole = new List<SystemUserImportUserRole>();
            List<string> errors = new List<string>();
            using (var fix = new SqlDatabaseFixture())
            {

                foreach (var user in users)
                {
                    //组织架构是否存在
                    var orgObj = await fix.Db.SystemOrganization.FindAsync(f => f.ParentIdsName == user.ParentIdsName.Trim());
                    if (orgObj == null)
                    {
                        errors.Add("组织:" + user.ParentIdsName + "不存在");
                    }

                    //检查账号是否存在
                    var count = await CountAsync(c => c.Code == user.Code.Trim());
                    if (count > 0)
                    {
                        errors.Add("账号:" + user.Code + "已存在");
                    }

                    SystemUserInfo input = new SystemUserInfo();
                    //新增
                    if (!user.Code.IsNullOrEmpty())
                    {
                        input.Password = DEncryptUtil.Encrypt("123456", _configOptions.Value.PasswordKey);
                    }

                    input.UserId = CombUtil.NewComb();
                    if (user.Role.IsNotNullOrEmpty())
                    {
                        foreach (var item in user.Role.Split(","))
                        {
                            var role = await fix.Db.SystemRole.FindAsync(f => f.Name == item.Trim());
                            if (role == null)
                            {
                                errors.Add("角色:" + item.Trim() + "已存在");
                            }
                            else
                            {
                                userRole.Add(new SystemUserImportUserRole
                                {
                                    UserId = input.UserId,
                                    RoleId = role.RoleId
                                });
                            }
                        }
                    }
                    input.Code = user.Code.Trim();
                    input.Name = user.Name.Trim();
                    input.Mobile = user.Mobile.Trim();
                    input.OtherContactInformation = user.OtherContactInformation;
                    if (orgObj != null)
                    {
                        input.OrganizationId = orgObj.OrganizationId;
                        input.OrganizationName = orgObj.Name;
                    }
                    systemUserSaveInputs.Add(input);
                }
            }
            if (errors.Any())
            {
                operateStatus.Data = errors;
                return operateStatus;
            }
            var operateStatusResult = new OperateStatus();
            foreach (var userInfoMap in systemUserSaveInputs)
            {
                operateStatusResult = await InsertAsync(userInfoMap);
                if (operateStatusResult.Code == ResultCode.Success)
                {
                    operateStatus.Msg = Chs.Successful;
                    operateStatus.Code = ResultCode.Success;
                    //添加用户到组织机构
                    await _permissionUserLogic.SavePermissionMasterValueBeforeDelete(EnumPrivilegeMaster.组织架构, userInfoMap.UserId, new List<Guid> { userInfoMap.OrganizationId });
                    await _permissionUserLogic.SavePermissionMasterValueBeforeDelete(EnumPrivilegeMaster.人员, userInfoMap.UserId, new List<Guid> { userInfoMap.UserId });
                    //判断是否具有角色
                    var role = userRole.Where(w => w.UserId == userInfoMap.UserId);
                    if (role.Count() > 0)
                    {
                        await _permissionUserLogic.SavePermissionMasterValueBeforeDelete(EnumPrivilegeMaster.角色, userInfoMap.UserId, role.Select(s => s.RoleId).ToList());
                    }
                }
                else
                {
                    return operateStatus;
                }
            }
            return operateStatus;
        }
    }
}