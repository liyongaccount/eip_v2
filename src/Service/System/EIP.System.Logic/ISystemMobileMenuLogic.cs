using EasyCaching.Core.Interceptor;
using EIP.Base.Models.Entities.System;
using EIP.Common.Logic;
using EIP.Common.Models;
using EIP.Common.Models.Dtos;
using EIP.Common.Models.Paging;
using EIP.Common.Models.Tree;
using EIP.System.Models.Dtos.MobileMenu;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace EIP.System.Logic.Permission.ILogic
{
    /// <summary>
    /// 移动端菜单
    /// </summary>
    public interface ISystemMobileMenuLogic : IAsyncLogic<SystemMobileMenu>
    {
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="input">移动端菜单</param>
        /// <returns></returns>
        [EasyCachingEvict(IsAll = true, CacheKeyPrefix = "ISystemMobileMenuLogic_Cache")]
        Task<OperateStatus> Save(SystemMobileMenu input);

        /// <summary>
        /// 菜单树
        /// </summary>
        /// <returns></returns>
        [EasyCachingAble(CacheKeyPrefix = "ISystemMobileMenuLogic_Cache")]
        Task<OperateStatus<IEnumerable<BaseTree>>> Tree();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [EasyCachingAble(CacheKeyPrefix = "ISystemMobileMenuLogic_Cache")]
        Task<IEnumerable<SystemMobileMenu>> FindMeunuByPId(IdInput input);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [EasyCachingEvict(IsAll = true, CacheKeyPrefix = "ISystemMobileMenuLogic_Cache")]
        Task<OperateStatus> Delete(IdInput<string> input);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [EasyCachingAble(CacheKeyPrefix = "ISystemMobileMenuLogic_Cache")]
        Task<IEnumerable<BaseTree>> FindHaveMenuButtonPermissionMenu();

        /// <summary>
        /// 获取显示在模块列表上数据
        /// </summary>
        /// <returns></returns>
        [EasyCachingAble(CacheKeyPrefix = "ISystemMobileMenuLogic_Cache")]
        Task<OperateStatus<PagedResults<SystemMobileMenuFindOutput>>> Find(SystemMobileMenuFindInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [EasyCachingAble(CacheKeyPrefix = "ISystemMobileMenuLogic_Cache")]
        Task<OperateStatus<SystemMobileMenu>> FindById(IdInput input);

        /// <summary>
        /// 是否具有模块权限
        /// </summary>
        /// <param name="input">Id</param>
        /// <returns></returns>
        [EasyCachingEvict(IsAll = true, CacheKeyPrefix = "ISystemMobileMenuLogic_Cache")]
        Task<OperateStatus> HaveMenuPermission(IdInput input);

        /// <summary>
        /// 是否具有数据权限
        /// </summary>
        /// <param name="input">Id</param>
        /// <returns></returns>
        [EasyCachingEvict(IsAll = true, CacheKeyPrefix = "ISystemMobileMenuLogic_Cache")]
        Task<OperateStatus> HaveDataPermission(IdInput input);

        /// <summary>
        /// 是否具有字段权限
        /// </summary>
        /// <param name="input">Id</param>
        /// <returns></returns>
        [EasyCachingEvict(IsAll = true, CacheKeyPrefix = "ISystemMobileMenuLogic_Cache")]
        Task<OperateStatus> HaveFieldPermission(IdInput input);

        /// <summary>
        /// 是否具有功能项权限
        /// </summary>
        /// <param name="input">Id</param>
        /// <returns></returns>
        [EasyCachingEvict(IsAll = true, CacheKeyPrefix = "ISystemMobileMenuLogic_Cache")]
        Task<OperateStatus> HaveButtonPermission(IdInput input);

        /// <summary>
        /// 冻结
        /// </summary>
        /// <param name="input">Id</param>
        /// <returns></returns>
        [EasyCachingEvict(IsAll = true, CacheKeyPrefix = "ISystemMobileMenuLogic_Cache")]
        Task<OperateStatus> IsFreeze(IdInput input);
    }
}
