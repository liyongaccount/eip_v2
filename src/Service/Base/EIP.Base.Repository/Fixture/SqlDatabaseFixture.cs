﻿/**************************************************************
* Copyright (C) 2018 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2018/10/30 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Common.Models.Resx;
using EIP.Common.Util;
using MySqlConnector;
using Npgsql;
using System;
using System.Data.SqlClient;

namespace EIP.Base.Repository.Fixture
{
    /// <summary>
    /// 
    /// </summary>
    public class SqlDatabaseFixture : IDisposable
    {
        /// <summary>
        /// 
        /// </summary>
        public SqlDatabaseFixture(bool read = true)
        {
            string connectionString;
            //if (read)
            //{
            //    //得到读连接字符串，此处还需加入负载算法
            //    connectionString = ConfigurationUtil.GetSection<ConnectionString>("EIP").SlaveConnectionString[0].ConnectionString;
            //}
            //else
            //{
            connectionString = ConfigurationUtil.GetSection("EIP:ConnectionString");
            //}
            var connectionType = ConfigurationUtil.GetSection("EIP:ConnectionType").ToLower();
            switch (connectionType)
            {
                case ResourceDataBaseType.Mysql:
                    Db = new SqlDbContext(new MySqlConnection(connectionString));
                    break;
                case ResourceDataBaseType.Postgresql:
                    Db = new SqlDbContext(new NpgsqlConnection(connectionString));
                    break;
                default://mssql
                    Db = new SqlDbContext(new SqlConnection(connectionString));
                    break;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public SqlDbContext Db { get; }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            Db.Dispose();
        }
    }
}