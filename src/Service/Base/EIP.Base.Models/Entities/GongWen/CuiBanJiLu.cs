﻿using EIP.Common.Models.Attributes.MicroOrm;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace EIP.Base.Models.Entities.GongWen
{
    /// <summary>
    /// 代码生成器表实体类
    /// </summary>
    [Serializable]
    [Table("eip_cuibanjilu")]
    public class CuiBanJiLu
    {
        /// <summary>
        /// 自增Id
        /// </summary>
        [Key, Identity, JsonIgnore]
        public int Id { get; set; }

        /// <summary>
        /// 主键
        /// </summary>
        public Guid RelationId { get; set; }

        /// <summary>
        /// 催办人
        /// </summary>
        public string CuiBanRen { get; set; }

        /// <summary>
        /// 催办人
        /// </summary>
        public string CuiBanRen_Txt { get; set; }

        /// <summary>
        /// 催办类型
        /// </summary>
        public string CuiBanLeiXing { get; set; }

        /// <summary>
        /// 催办类型
        /// </summary>
        public string CuiBanLeiXing_Txt { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid CreateUserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CreateUserName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid ProcessInstanceId { get; set; }
    }
}
