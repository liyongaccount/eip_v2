﻿using EIP.Common.Models.Attributes.MicroOrm;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace EIP.Base.Models.Entities.WeChat
{
    /// <summary>
    /// 企业微信组织架构
    /// </summary>
    [Serializable]
    [Table("WeChat_Work_Department")]
    public class WeChatWorkDepartment
    {
        /// <summary>
        /// 自增Id
        /// </summary>
        [Key, Identity, JsonIgnore]
        public int Id { get; set; }

        /// <summary>
        /// 主键Id
        /// </summary>
        public Guid DepartmentId { get; set; }

        /// <summary>
        /// 企业微信组织Id
        /// </summary>
        public long? DeptId { get; set; }

        /// <summary>
        /// 企业微信组织名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 企业微信父级Id
        /// </summary>
        public long? ParentId { get; set; }

        /// <summary>
        /// 对应系统Id
        /// </summary>
        public Guid OrganizationId { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>		
        public DateTime CreateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 创建用户Id
        /// </summary>		
        public Guid CreateUserId { get; set; }

        /// <summary>
        /// 创建人员名称
        /// </summary>
        public string CreateUserName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>		
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 创建用户Id
        /// </summary>		
        public Guid? UpdateUserId { get; set; }

        /// <summary>
        /// 修改人员名称
        /// </summary>
        public string UpdateUserName { get; set; }
    }

}
