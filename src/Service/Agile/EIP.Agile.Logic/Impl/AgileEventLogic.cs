﻿/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/

using EIP.Agile.Models.Dtos.Event;
using EIP.Common.Util;
using System.Threading.Tasks;

namespace EIP.Agile.Logic.Impl
{
    /// <summary>
    /// 
    /// </summary>
    public class AgileEventLogic : IAgileEventLogic
    {
        /// <summary>
        /// 根据Api获取数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public object EventDoByApi(AgileEventDoByApiInput input)
        {
            input.Url = ConfigurationUtil.GetSection("EIP:ApiServer") + input.Url;
            switch (input.Type.ToLower())
            {
                case "0":
                    return  RequestUtil.EventDoByApiPost(input.Url, new
                    {
                        Param = input.Param
                    }, input.Header);
                default:
                    return  RequestUtil.EventDoByApiGet(input.Url, null, input.Header);
            }
        }
    }
}