﻿/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Agile.Models.Dtos.DataBase;
using EIP.Agile.Repository;
using EIP.Base.Models.Entities.Agile;
using EIP.Common.Logic;
using EIP.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EIP.Agile.Logic.Impl
{
    /// <summary>
    /// 数据来源操作
    /// </summary>
    public class AgileDataSourceLogic : DapperAsyncLogic<AgileDataSource>, IAgileDataSourceLogic
    {
        #region 构造函数

        private readonly IAgileDataSourceRepository _systemDataSourceRepository;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="systemDataSourceRepository"></param>
        public AgileDataSourceLogic(IAgileDataSourceRepository systemDataSourceRepository)
        {
            _systemDataSourceRepository = systemDataSourceRepository;
        }
        #endregion

        /// <summary>
        /// 获取对应表列信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<OperateStatus<IEnumerable<AgileDataBaseColumnDto>>> FindDataBaseColumns(AgileDataBaseTableDto input)
        {
            return OperateStatus<IEnumerable<AgileDataBaseColumnDto>>.Success(await _systemDataSourceRepository.FindDataBaseColumns(input));
        }

        /// <summary>
        /// 获取对应数据库表信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<OperateStatus<IEnumerable<AgileDataBaseTableDto>>> FindDataBaseProc()
        {
            return OperateStatus<IEnumerable<AgileDataBaseTableDto>>.Success(await _systemDataSourceRepository.FindDataBaseProc());
        }

        /// <summary>
        /// 查看对应数据库空间占用情况
        /// </summary>
        /// <returns></returns>
        public async Task<OperateStatus<IEnumerable<AgileDataBaseSpaceOutput>>> FindDataBaseSpaceused()
        {
            return OperateStatus<IEnumerable<AgileDataBaseSpaceOutput>>.Success(await _systemDataSourceRepository.FindDataBaseSpaceused());
        }

        /// <summary>
        /// 获取对应数据库表信息
        /// </summary>
        /// <returns></returns>
        public async Task<OperateStatus<IEnumerable<AgileDataBaseTableDto>>> FindDataBaseTable()
        {
            return OperateStatus<IEnumerable<AgileDataBaseTableDto>>.Success(await _systemDataSourceRepository.FindDataBaseTable());
        }

        /// <summary>
        /// 获取对应数据库表信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<OperateStatus<IEnumerable<AgileDataBaseTableDto>>> FindDataBaseView()
        {
            return OperateStatus<IEnumerable<AgileDataBaseTableDto>>.Success(await _systemDataSourceRepository.FindDataBaseView());
        }

        /// <summary>
        /// 获取外键信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<OperateStatus<IEnumerable<AgileDataBaseFkColumnOutput>>> FinddatabsefFkColumn(AgileDataBaseTableDto input)
        {
            return OperateStatus<IEnumerable<AgileDataBaseFkColumnOutput>>.Success(await _systemDataSourceRepository.FinddatabsefFkColumn(input));
        }

    }
}