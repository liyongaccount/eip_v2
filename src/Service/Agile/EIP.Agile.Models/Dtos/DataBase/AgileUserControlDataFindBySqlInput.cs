﻿/**************************************************************
* Copyright (C) 2018 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2019/6/6 21:47:11
* 文件名: SystemUserControlDataFindBySqlInput
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using System;

namespace EIP.Agile.Models.Dtos.DataBase
{
    /// <summary>
    /// 
    /// </summary>
    public class AgileUserControlDataFindBySqlInput
    {
        /// <summary>
        /// 归属库
        /// </summary>
        public Guid DataBaseId { get; set; }

        /// <summary>
        /// Sql语句
        /// </summary>
        public string Sql { get; set; }
    }
}
