﻿using System;

namespace EIP.Agile.Models.Dtos.Config
{
    /// <summary>
    /// 
    /// </summary>
    public class AgileConfigFindByMenuIdInput
    {
        /// <summary>
        /// 
        /// </summary>
        public Guid MenuId { get; set; }

        /// <summary>
        /// 生成类型:1列表配置,2表单配置,3卡片配置
        /// </summary>
        public int? ConfigType { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class AgileConfigFindByMenuIdOutput
    {
        /// <summary>
        /// 
        /// </summary>
        public Guid ConfigId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Theme { get; set; }

        /// <summary>
        /// 使用类型:1敏捷开发,2流程表单
        /// </summary>
        public short? UseType { get; set; }

        /// <summary>
        /// 分类Id
        /// </summary>
        public Guid? TypeId { get; set; }
    }
}
