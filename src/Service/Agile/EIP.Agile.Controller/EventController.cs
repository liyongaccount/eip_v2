﻿/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/

using EIP.Agile.Logic;
using EIP.Agile.Models.Dtos.Event;
using EIP.Common.Controller.Attribute;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace EIP.Agile.Controller
{
    /// <summary>
    /// 事件处理器
    /// </summary>
    public class EventController : BaseAgileController
    {
        #region 构造函数
        private readonly IAgileEventLogic _agileEventLogic;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="agileEventLogic"></param>
        public EventController(IAgileEventLogic agileEventLogic)
        {
            _agileEventLogic = agileEventLogic;
        }

        #endregion

        #region 方法

        /// <summary>
        /// 根据Api处理事件
        /// </summary>
        /// <param name="input">Id</param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("事件处理-方法-根据Api处理事件", RemarkFrom.System)]
        [Route("/agile/event/api")]
        public object EventDoByApi(AgileEventDoByApiInput input)
        {
            input.Header = "Authorization:" + Request.Headers["Authorization"];
            return  _agileEventLogic.EventDoByApi(input);
        }

        #endregion
    }
}