using EIP.Agile.Models.Dtos.Config;
using EIP.Base.Models.Entities.Agile;
using EIP.Common.Extension;
using EIP.Common.Models.Paging;
using EIP.Common.Repository;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EIP.Agile.Repository.Impl
{
    /// <summary>
    ///  敏捷开发
    /// </summary>
    public class AgileConfigRepository : IAgileConfigRepository
    {
        /// <summary>
        /// 获取操作日志
        /// </summary>
        /// <param name="paging"></param>
        /// <returns></returns>
        public Task<PagedResults<AgileConfig>> Find(AgileConfigFindInput paging)
        {
            var sql = new StringBuilder($@"SELECT ConfigId
                                                 ,Name
                                                 ,DataFrom
                                                 ,IsFreeze
                                                 ,Remark
                                                 ,OrderNo
                                                 ,FormCategory
                                                 ,EditConfigId
                                                 ,UseType
                                                 ,CreateTime
                                                 ,CreateUserName
                                                 ,UpdateTime
                                                 ,UpdateUserName
                                                 ,@rowNumber 
                                                 ,@recordCount FROM Agile_Config @where and IsDelete=0 ");
            if (paging.Sidx.IsNullOrEmpty())
            {
                paging.Sidx = " OrderNo ";
            }
            if (paging.TypeId.HasValue)
            {
                sql.Append($" and TypeId='{paging.TypeId}' ");
            }
            if (paging.ConfigType.HasValue)
            {
                sql.Append($" and ConfigType={paging.ConfigType} ");
            }
            if (paging.FormCategory.HasValue)
            {
                sql.Append($" and FormCategory={paging.FormCategory} ");
            }
            if (paging.MenuIdNull)
            {
                sql.Append($" and menuId is null  ");
            }
            if (paging.UseType.HasValue)
            {
                sql.Append($" and UseType={paging.UseType} ");
            }
            if (paging.ConfigId.HasValue)
            {
                sql.Append($" and ConfigId='{paging.ConfigId}' ");
            }
            return new SqlMapperUtil().PagingQuerySqlAsync<AgileConfig>(sql.ToString(), paging);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public Task<IEnumerable<AgileConfigFindBaseOutput>> FindBase(AgileConfigFindBaseInput input)
        {
            StringBuilder sql = new StringBuilder($@"select ConfigId,Name from Agile_Config config where IsFreeze=0 and IsDelete=0 ");
            if (input.ConfigType.HasValue)
            {
                sql.Append($" and ConfigType={input.ConfigType} ");
            }
            if (input.UseType.HasValue)
            {
                sql.Append($" and UseType={input.UseType} ");
            }
            if (input.ConfigId.HasValue)
            {
                sql.Append($" and ConfigId='{input.ConfigId}' ");
            }
            return new SqlMapperUtil().SqlWithParams<AgileConfigFindBaseOutput>(sql.ToString());
        }
    }
}