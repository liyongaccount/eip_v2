﻿using EIP.Agile.Models.Dtos.Config;
using EIP.Base.Models.Entities.Agile;
using EIP.Common.Models.Paging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EIP.Agile.Repository
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAgileConfigRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="paging"></param>
        /// <returns></returns>
        Task<PagedResults<AgileConfig>> Find(AgileConfigFindInput paging);

        /// <summary>
        /// 获取基础
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<IEnumerable<AgileConfigFindBaseOutput>> FindBase(AgileConfigFindBaseInput input);
    }
}