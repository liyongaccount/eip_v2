/**************************************************************
* Copyright (C) 2018 www.sf-info.cn 盛峰版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2018/10/30 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/

using EIP.Common.Extension;
using EIP.Common.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EIP.Workflow.Engine.Repository
{
    /// <summary>
    /// 
    /// </summary>
    internal class WorkflowProcessInstanceLinkRepository : IWorkflowProcessInstanceLinkRepository
    {
        /// <summary>
        /// 更新通过状态
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<int> UpdateIsPass(List<Guid> input)
        {
            return await new SqlMapperUtil().InsertUpdateOrDeleteSql<int>($"UPDATE Workflow_ProcessInstance_Link SET IsPass=1 WHERE LinkId IN ({input.Select(s => s).ExpandAndToString().InSql()})");
        }
    }
}