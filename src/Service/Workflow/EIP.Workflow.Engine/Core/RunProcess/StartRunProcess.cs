﻿/**************************************************************
* Copyright (C) 2018 www.sf-info.cn 盛峰版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2018/10/30 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Common.Models;

namespace EIP.Workflow.Engine.Core.RunProcess
{
    internal class StartRunProcess : BaseRunProcess
    {
        /// <summary>
        /// 开始流程
        /// 
        /// </summary>
        /// <returns></returns>
        internal override OperateStatus RunProcess()
        {
            OperateStatus operateStatus = new OperateStatus();

            return operateStatus;
        }

        ///// <summary>
        ///// 开始流程实例:插入活动及连线
        ///// 查询对应的活动类型
        ///// </summary>
        ///// <param name="input">流程Id</param>
        ///// <returns></returns>
        //private async Task<OperateStatus> StartProcessInstance()
        //{

        //}
    }
}
