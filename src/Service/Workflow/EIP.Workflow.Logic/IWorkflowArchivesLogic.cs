﻿/**************************************************************
* Copyright (C) 2018 www.sf-info.cn 盛峰版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2018/10/30 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EasyCaching.Core.Interceptor;
using EIP.Base.Models.Entities.Workflow;
using EIP.Common.Logic;
using EIP.Common.Models;
using EIP.Common.Models.Paging;
using EIP.Workflow.Models.Dtos.Archives;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using System.Threading.Tasks;

namespace EIP.Workflow.Logic.ILogic
{
    /// <summary>
    /// 工作流处理归档接口定义
    /// </summary>
    public interface IWorkflowArchivesLogic : IAsyncLogic<WorkflowArchive>
    {
        /// <summary>
        /// 保存归档信息
        /// </summary>
        /// <param name="input">归档信息</param>
        /// <returns></returns>
        [EasyCachingEvict(IsAll = true, CacheKeyPrefix = "IWorkflowArchivesLogic_Cache")]
        Task<OperateStatus> Save(WorkflowArchive input);

        /// <summary>
        /// 获取归档数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [EasyCachingAble(CacheKeyPrefix = "IWorkflowArchivesLogic_Cache")]
        Task<PagedResults<WorkflowArchiveOutput>> Find(WorkflowArchiveInput input);
    }
}