﻿using EIP.Workflow.Job.Entities;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.DbContext;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using System.Data.SqlClient;

namespace EIP.Workflow.Job.Fixture
{
    /// <summary>
    /// 
    /// </summary>
    public class MsSqlDbContext : DapperDbContext, IDbContext
    {
        private readonly SqlGeneratorConfig _config = new SqlGeneratorConfig
        {
            UseQuotationMarks = false,
            SqlProvider = SqlProvider.MSSQL
        };

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionString"></param>
        public MsSqlDbContext(string connectionString)
            : base(new SqlConnection(connectionString))
        {
        }

        /// <summary>
        /// 测试
        /// </summary>
        private IDapperRepository<FormDemo> _formDemo;
        /// <summary>
        /// 测试
        /// </summary>
        public IDapperRepository<FormDemo> FormDemo => _formDemo ?? (_formDemo = new DapperRepository<FormDemo>(Connection, _config));

    }
}