﻿using EIP.Workflow.Job.Entities;
using MicroOrm.Dapper.Repositories;

namespace EIP.Workflow.Job.Fixture
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDbContext
    {
        /// <summary>
        /// 测试
        /// </summary>
        IDapperRepository<FormDemo> FormDemo { get; }
    }
}