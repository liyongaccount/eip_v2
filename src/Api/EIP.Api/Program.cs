using AspNetCoreRateLimit;
using AspNetCoreRateLimit.Redis;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using EasyCaching.Core;
using EasyCaching.Interceptor.Castle;
using EIP.Api.Middleware;
using EIP.Base.Models.Entities.System;
using EIP.Common.Controller.Filter;
using EIP.Common.Controller.Middlewares;
using EIP.Common.Controller.Provider;
using EIP.Common.Core.Context;
using EIP.Common.JwtAuthorize;
using EIP.Common.Message.WebSite;
using EIP.Common.Models.Dtos;
using EIP.Common.Models.Resx;
using EIP.Common.Models.Tree;
using EIP.Common.Util;
using EIP.System.Models.Dtos.Role;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.DependencyModel;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using NLog;
using NLog.Web;
using Senparc.CO2NET.RegisterServices;
using Senparc.Weixin.RegisterServices;
using StackExchange.Redis;
using System.Reflection;
using System.Runtime.Loader;

var builder = WebApplication.CreateBuilder(args);

#region 配置文件
ConfigurationUtil.Configuration = builder.Configuration;
#endregion

#region 跨域
builder.Services.AddCors(options =>
{
    options.AddPolicy("EIPCors", policy => policy.WithOrigins(builder.Configuration.GetSection("Cors")["Origins"].Split(',')).AllowAnyHeader().AllowAnyMethod().AllowCredentials());
});
#endregion

#region Swagger
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc(ConfigurationUtil.Configuration["Swagger:Name"], new OpenApiInfo
    {
        Version = ConfigurationUtil.Configuration["Swagger:Version"],
        Title = ConfigurationUtil.Configuration["Swagger:Title"],
        Description = ConfigurationUtil.Configuration["Swagger:Description"],
    });
    var basePath = PlatformServices.Default.Application.ApplicationBasePath;
    c.IncludeXmlComments(Path.Combine(basePath, "EIP.System.Controller.xml"));
    c.IncludeXmlComments(Path.Combine(basePath, "EIP.System.Models.xml"));
    c.IncludeXmlComments(Path.Combine(basePath, "EIP.Job.Controller.xml"));
    c.IncludeXmlComments(Path.Combine(basePath, "EIP.Workflow.Controller.xml"));
    c.IncludeXmlComments(Path.Combine(basePath, "EIP.Workflow.Models.xml"));
    c.IncludeXmlComments(Path.Combine(basePath, "EIP.Agile.Controller.xml"));
    c.IncludeXmlComments(Path.Combine(basePath, "EIP.Agile.Models.xml"));
    //这里是给Swagger添加验证的部分
    // JWT认证                                                 
    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        Scheme = "bearer",
        BearerFormat = "JWT",
        Type = SecuritySchemeType.Http,
        Name = "Authorization",
        In = ParameterLocation.Header,
        Description = "Authorization:Bearer 请输入带有Bearer的Token<br/><b></b>",
    });
    c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme{Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[] { }
                    }
                });
});
#endregion

#region Jwt
builder.Services.AddApiJwtAuthorize((context) =>
{
    return true;
});
#endregion

#region 注册Mapper
RegisterMapper();
#endregion

#region 缓存
builder.Services.AddEasyCaching(options =>
{
    options.UseInMemory();
    //options.UseRedis(config =>
    //{
    //    config.DBConfig = new RedisDBOptions { Configuration = "localhost" };
    //});
    //options.WithJson(config => { config.TypeNameHandling = Newtonsoft.Json.TypeNameHandling.None; });
    //options.WithMessagePack();
    //options.WithProtobuf();
});
builder.Services.ConfigureCastleInterceptor(options => options.CacheProviderName = EasyCachingConstValue.DefaultInMemoryName);
#endregion

#region  EIP基础配置
builder.Services.Configure<EIPConfig>(ConfigurationUtil.Configuration.GetSection("EIP"));
#endregion

builder.Services.AddSession();//使用Session

#region Mvc
//配置可以同步请求读取流数据
builder.Services.Configure<KestrelServerOptions>(x => x.AllowSynchronousIO = true)
    .Configure<IISServerOptions>(x => x.AllowSynchronousIO = true);
builder.Services.AddMvc(options =>
{
    options.Filters.Add<ExceptionFilter>();
    options.Filters.Add<ModelStateFilter>();
    options.Filters.Add<ActionResultFilter>();
    options.ModelMetadataDetailsProviders.Add(new RequiredBindingMetadataProvider());
}).AddJsonOptions(options =>
{

}) .AddApplicationPart(Assembly.Load(new AssemblyName("EIP.Agile.Controller")))
    .AddApplicationPart(Assembly.Load(new AssemblyName("EIP.Workflow.Controller")))
    .AddApplicationPart(Assembly.Load(new AssemblyName("EIP.Job.Controller")))
    .AddApplicationPart(Assembly.Load(new AssemblyName("EIP.System.Controller")));


builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
builder.Services.AddTokenJwtAuthorize();

builder.Services.AddControllers().AddJsonOptions(options => options.JsonSerializerOptions.PropertyNamingPolicy = null).AddNewtonsoftJson(opt =>
{
    opt.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
    //忽略循环引用
    opt.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
});
#endregion

#region 微信
builder.Services.AddSenparcGlobalServices(ConfigurationUtil.Configuration)
   .AddSenparcWeixinServices(ConfigurationUtil.Configuration);
#endregion

#region 验证码
builder.Services.AddCaptcha(ConfigurationUtil.Configuration);
// 如果使用redis分布式缓存
//builder.Services.AddStackExchangeRedisCache(options =>
//{
//    options.Configuration = builder.Configuration.GetConnectionString("RedisCache");
//    options.InstanceName = "captcha:";
//});
#endregion

#region Autofac
builder.Host.UseServiceProviderFactory(new AutofacServiceProviderFactory());
builder.Host.ConfigureContainer<ContainerBuilder>(ConfigureContainer);
#endregion

#region SignalR
builder.Services.AddSignalR();
#endregion

builder.Configuration.AddJsonFile($"ratelimitconfig.json", optional: true, reloadOnChange: true);

#region NLog
var configName = "";
var connectionType = ConfigurationUtil.Configuration.GetSection("EIP:ConnectionType").Value.ToLower();
switch (connectionType)
{
    case ResourceDataBaseType.Mysql:
        configName = "nlog.mysql.config";
        break;
    default:
        configName = "nlog.config";
        break;
}
var logger = NLog.LogManager.Setup().LoadConfigurationFromFile(configName).GetCurrentClassLogger();
builder.Logging.ClearProviders();
builder.Logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Information);
builder.Host.UseNLog();
LogManager.Configuration.Variables["connectionString"] = ConfigurationUtil.Configuration.GetSection("EIP_Log:ConnectionString").Value;
#endregion

#region 限流
builder.Services.Configure<IpRateLimitOptions>(ConfigurationUtil.Configuration.GetSection("IpRateLimiting"));
builder.Services.Configure<IpRateLimitPolicies>(ConfigurationUtil.Configuration.GetSection("IpRateLimitPolicies"));

//需要存储速率限制计算器和ip规则
builder.Services.AddSingleton<IIpPolicyStore, DistributedCacheIpPolicyStore>();
builder.Services.AddSingleton<IRateLimitCounterStore, DistributedCacheRateLimitCounterStore>();

var redisOptions = ConfigurationOptions.Parse(ConfigurationUtil.Configuration["EIP_Redis:ConnectionString"]);
builder.Services.AddSingleton<IConnectionMultiplexer>(provider => ConnectionMultiplexer.Connect(redisOptions));
builder.Services.AddRedisRateLimiting();
//配置（解析器、计数器密钥生成器）
builder.Services.AddSingleton<IRateLimitConfiguration, RateLimitConfiguration>();
#endregion

var app = builder.Build();
app.UseSession();
//使用静态上下文
app.UseStaticHttpContext();

#region 跨域相关
app.UseCors("EIPCors");
#endregion

app.UseSession();
//使用静态上下文
app.UseStaticHttpContext();

#region Swagger
app.UseSwagger();
app.UseSwaggerUI(options =>
{
    //options.SwaggerEndpoint("/Api/swagger/" + Configuration["Swagger:Name"] + "/swagger.json", Configuration["Swagger:Description"]);
    options.SwaggerEndpoint("/swagger/" + ConfigurationUtil.Configuration["Swagger:Name"] + "/swagger.json", ConfigurationUtil.Configuration["Swagger:Description"]);
});
#endregion

#region 错误中间件
app.UseErrorHandlingMiddleware();
#endregion

#region 请求中间件
app.UseRequestProviderMiddleware();
#endregion

#region 限流
app.UseMiddleware<CustomizationLimitMiddleware>();
#endregion

app.UseRouting();
app.UseAuthorization();
app.UseStaticFiles();
app.MapHub<WebSiteHub>("/eiphub");

app.MapControllerRoute(
       name: "default",
       pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();

/// <summary>
/// Autofac注册
/// </summary>
/// <param name="builder"></param>
void ConfigureContainer(ContainerBuilder builder)
{
    var assemblys = GetAllAssemblies();
    builder.RegisterAssemblyTypes(assemblys.ToArray()).Where(t => t.Name.EndsWith("Logic")).AsImplementedInterfaces();
    builder.RegisterAssemblyTypes(assemblys.ToArray()).Where(t => t.Name.EndsWith("Repository")).AsImplementedInterfaces();
    builder.ConfigureCastleInterceptor();
}

/// <summary>
/// 获取项目程序集
/// </summary>
/// <returns></returns>
static IList<Assembly> GetAllAssemblies()
{
    var list = new List<Assembly>();
    var deps = DependencyContext.Default;
    var libs = deps.CompileLibraries;
    foreach (var lib in libs)
    {
        try
        {
            var assembly = AssemblyLoadContext.Default.LoadFromAssemblyName(new AssemblyName(lib.Name));
            list.Add(assembly);
        }
        catch (Exception)
        {

        }
    }
    return list;
}
/// <summary>
/// Mapper注册
/// </summary>
void RegisterMapper()
{
    Mapper.Initialize(config =>
    {
        config.CreateMap<SystemMenu, BaseTree>()
            .ForMember(o => o.id, opt => opt.MapFrom(src => src.MenuId))
            .ForMember(o => o.parent, opt => opt.MapFrom(src => src.ParentId))
            .ForMember(o => o.text, opt => opt.MapFrom(src => src.Name));
        config.CreateMap<SystemMobileMenu, BaseTree>()
            .ForMember(o => o.id, opt => opt.MapFrom(src => src.MobileMenuId))
            .ForMember(o => o.parent, opt => opt.MapFrom(src => src.ParentId))
            .ForMember(o => o.text, opt => opt.MapFrom(src => src.Name));
        config.CreateMap<SystemDictionary, BaseTree>()
           .ForMember(o => o.id, opt => opt.MapFrom(src => src.DictionaryId))
           .ForMember(o => o.parent, opt => opt.MapFrom(src => src.ParentId))
           .ForMember(o => o.text, opt => opt.MapFrom(src => src.Name));
        config.CreateMap<SystemOrganization, BaseTree>()
            .ForMember(o => o.id, opt => opt.MapFrom(src => src.OrganizationId))
            .ForMember(o => o.parent, opt => opt.MapFrom(src => src.ParentId))
            .ForMember(o => o.text, opt => opt.MapFrom(src => src.Name));
        config.CreateMap<SystemRole, SystemRoleSelectOutput>()
            .ForMember(o => o.RoleId, opt => opt.MapFrom(src => src.RoleId))
            .ForMember(o => o.Name, opt => opt.MapFrom(src => src.Name));
    });
}