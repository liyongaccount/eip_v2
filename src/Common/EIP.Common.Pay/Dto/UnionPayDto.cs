﻿using System.ComponentModel.DataAnnotations;

namespace EIP.Common.Pay.Dto
{
    public class GatewayPayFrontConsumeFrontConsumeDto
    {
        
        [Display(Name = "orderId")]
        public string OrderId { get; set; }

        
        [Display(Name = "txnTime")]
        public string TxnTime { get; set; }

        
        [Display(Name = "txnAmt")]
        public string TxnAmt { get; set; }

        
        [Display(Name = "currencyCode")]
        public string CurrencyCode { get; set; }

        [Display(Name = "payTimeout")]
        public string PayTimeOut { get; set; }

        [Display(Name = "frontUrl")]
        public string FrontUrl { get; set; }

        
        [Display(Name = "backUrl")]
        public string BackUrl { get; set; }
    }
}
