﻿/**************************************************************
* Copyright (C) 2018 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2018/10/30 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace EIP.Common.Util
{
    public class ConfigurationUtil
    {
        public static IConfiguration Configuration;

        public static T GetSection<T>(string key) where T : class, new()
        {
            var obj = new ServiceCollection()
                .AddOptions()
                .Configure<T>(Configuration.GetSection(key))
                .BuildServiceProvider()
                .GetService<IOptions<T>>()
                .Value;
            return obj;
        }

        public static string GetSection(string key)
        {
            return Configuration.GetValue<string>(key);
        }

        /// <summary>
        /// 获取上传路径
        /// </summary>
        /// <returns></returns>
        public static string GetUploadPath()
        {
            return GetSection("EIP:UploadPath");
        }

        /// <summary>
        /// 文件服务器地址
        /// </summary>
        /// <returns></returns>
        public static string GetFileServer()
        {
            return GetSection("EIP:FileServer");
        }
    }
}