﻿/**************************************************************
* Copyright (C) 2018 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2018/10/30 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;

namespace EIP.Common.Util
{
    /// <summary>
    /// 请求工具
    /// </summary>
    public static class RequestUtil
    {
        /// <summary>
        /// 请求与响应的超时时间
        /// </summary>
        public static int Timeout { get; set; } = 100000;
        /// <summary>
        /// 执行HTTP POST请求。
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="data">请求参数:</param>
        /// <param name="header">请求头:"Authorization:Bearer xxxx"</param>
        /// <returns>HTTP响应</returns>
        public static string EventDoByApiPost(string url, object data, string header = null)
        {
            var req = GetWebRequest(url, "POST");
            WebHeaderCollection headers = req.Headers;
            if (header != null)
                headers.Add(header);
            req.ContentType = "application/json";
            req.Accept = "application/json";
            var postData = Encoding.UTF8.GetBytes(data == null ? string.Empty : JsonConvert.SerializeObject(data));
            var reqStream = req.GetRequestStream();
            reqStream.Write(postData, 0, postData.Length);
            reqStream.Close();
            var rsp = (HttpWebResponse)req.GetResponse();
            var encoding = Encoding.GetEncoding(rsp.CharacterSet);
            return GetResponseAsString(rsp, encoding);
        }
        /// <summary>
        /// 执行HTTP GET请求。
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="parameters">请求参数</param>
        /// <param name="charset">编码字符集</param>
        /// <param name="header">请求头:"Authorization:Bearer xxxx"</param>
        /// <returns>HTTP响应</returns>
        public static string EventDoByApiGet(string url, IDictionary<string, string> parameters, string charset = "utf-8", string header = null)
        {
            if (parameters != null && parameters.Count > 0)
                if (url.Contains("?"))
                    url = url + "&" + BuildQuery(parameters, charset);
                else
                    url = url + "?" + BuildQuery(parameters, charset);
            var req = GetWebRequest(url, "GET");
            WebHeaderCollection headers = req.Headers;
            if (header != null)
                headers.Add(header);
            req.ContentType = "application/x-www-form-urlencoded;charset=" + charset;
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            var rsp = (HttpWebResponse)req.GetResponse();
            var encoding = Encoding.GetEncoding(rsp.CharacterSet);
            return GetResponseAsString(rsp, encoding);
        }
        /// <summary>
        /// 执行HTTP POST请求。
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="data">请求参数:</param>
        /// <param name="header">请求头:"Authorization:Bearer xxxx"</param>
        /// <returns>HTTP响应</returns>
        public static async Task<string> Post(string url, object data, string header = null)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("header", header);
            HttpResponseMessage response = await client.PostAsync(url, new StringContent(JsonConvert.SerializeObject(data)));
            string reslut = await response.Content.ReadAsStringAsync();
            return reslut;
        }
       
        /// <summary>
        /// 执行HTTP GET请求。
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="parameters">请求参数</param>
        /// <param name="charset">编码字符集</param>
        /// <param name="header">请求头:"Authorization:Bearer xxxx"</param>
        /// <returns>HTTP响应</returns>
        public static async Task<string> Get(string url, IDictionary<string, string> parameters, string charset = "utf-8", string header = null)
        {
            if (parameters != null && parameters.Count > 0)
                if (url.Contains("?"))
                    url = url + "&" + BuildQuery(parameters, charset);
                else
                    url = url + "?" + BuildQuery(parameters, charset);
            //var req = GetWebRequest(url, "GET");
            //WebHeaderCollection headers = req.Headers;
            //if (header != null)
            //    headers.Add(header);
            //req.ContentType = "application/x-www-form-urlencoded;charset=" + charset;
            //Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            //var rsp = (HttpWebResponse)req.GetResponse();
            //var encoding = Encoding.GetEncoding(rsp.CharacterSet);
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("header", header);
            HttpResponseMessage response = await client.GetAsync(url);
            string reslut = await response.Content.ReadAsStringAsync();
            return reslut;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="method"></param>
        /// <param name="header">请求头:"Authorization:Bearer xxxx"</param>
        /// <returns></returns>
        public static HttpWebRequest GetWebRequest(string url, string method, string header = null)
        {
            var req = (HttpWebRequest)WebRequest.Create(url);
            WebHeaderCollection headers = req.Headers;
            if (header != null)
                headers.Add(header);
            req.Method = method;
            req.KeepAlive = true;
            req.UserAgent = "Aop4Net";
            req.Timeout = Timeout;
            return req;
        }

        /// <summary>
        /// 把响应流转换为文本。
        /// </summary>
        /// <param name="rsp">响应流对象</param>
        /// <param name="encoding">编码方式</param>
        /// <returns>响应文本</returns>
        public static string GetResponseAsString(HttpWebResponse rsp, Encoding encoding)
        {
            var result = new StringBuilder();
            Stream stream = null;
            StreamReader reader = null;
            try
            {
                // 以字符流的方式读取HTTP响应
                stream = rsp.GetResponseStream();
                reader = new StreamReader(stream, encoding);
                // 按字符读取并写入字符串缓冲
                var ch = -1;
                while ((ch = reader.Read()) > -1)
                {
                    // 过滤结束符
                    var c = (char)ch;
                    if (c != '\0')
                        result.Append(c);
                }
            }
            finally
            {
                // 释放资源
                reader?.Close();
                stream?.Close();
                rsp?.Close();
            }
            return result.ToString();
        }

        /// <summary>
        /// 组装普通文本请求参数。
        /// </summary>
        /// <param name="parameters">Key-Value形式请求参数字典</param>
        /// <param name="charset"></param>
        /// <returns>URL编码后的请求数据</returns>
        public static string BuildQuery(IDictionary<string, string> parameters, string charset = "utf-8")
        {
            var postData = new StringBuilder();
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            var hasParam = false;

            var dem = parameters.GetEnumerator();
            while (dem.MoveNext())
            {
                var name = dem.Current.Key;
                var value = dem.Current.Value;
                // 忽略参数名或参数值为空的参数
                if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(value))
                {
                    if (hasParam)
                        postData.Append("&");

                    postData.Append(name);
                    postData.Append("=");
                    var encodedValue = HttpUtility.UrlEncode(value, Encoding.GetEncoding(charset));
                    postData.Append(encodedValue);
                    hasParam = true;
                }
            }
            return postData.ToString();
        }
        /// <summary>
        /// 组装普通文本请求参数。
        /// </summary>
        /// <param name="parameters">Key-Value形式请求参数字典</param>
        /// <param name="charset"></param>
        /// <returns>URL编码后的请求数据</returns>
        public static string BuildQueryOpenApi(IDictionary<string, string> parameters)
        {
            var postData = new StringBuilder();
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            var hasParam = false;

            var dem = parameters.GetEnumerator();
            while (dem.MoveNext())
            {
                var name = dem.Current.Key;
                var value = dem.Current.Value;
                // 忽略参数名或参数值为空的参数
                if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(value))
                {
                    if (hasParam)
                        postData.Append("&");

                    postData.Append(name);
                    postData.Append("=");
                    postData.Append(value);
                    hasParam = true;
                }
            }
            return postData.ToString();
        }
        /// <summary>
        /// 执行HTTP POST请求。
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="data">请求参数:</param>
        /// <param name="header">请求头:"Authorization:Bearer xxxx"</param>
        /// <returns>HTTP响应</returns>
        public static string PostTfsOpenApi(string method, object data = null)
        {
            var url = ConfigurationUtil.GetSection("Tfs:OpenApi");
            var req = GetWebRequest(url, "POST");
            var appId = "XEI262335635873861RS";
            IDictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("appid", appId);
            if (data != null)
            {
                parameters.Add("data", JsonConvert.SerializeObject(data));
            }
            parameters.Add("method", method);
            parameters.Add("sign_type", "md5");
            parameters.Add("version", "1.0");
            parameters.Add("appsecret", "inlzegyshq0nygsuj5mrcbs8pg2ojdni");
            var sign = DEncryptUtil.Md5(BuildQueryOpenApi(parameters).ToLower()).ToLower();
            object postDataObj;
            if (data == null)
            {
                postDataObj = new
                {
                    method = method,
                    sign_type = "md5",
                    sign = sign,
                    appid = appId,
                    version = "1.0",
                };
            }
            else
            {
                postDataObj = new
                {
                    method = method,
                    sign_type = "md5",
                    sign = sign,
                    appid = appId,
                    version = "1.0",
                    data = data
                };
            }

            req.ContentType = "application/json";
            req.Accept = "application/json";
            var postData = Encoding.Default.GetBytes(postDataObj == null ? string.Empty : JsonConvert.SerializeObject(postDataObj));
            var reqStream = req.GetRequestStream();
            reqStream.Write(postData, 0, postData.Length);
            reqStream.Close();
            var rsp = (HttpWebResponse)req.GetResponse();
            var encoding = Encoding.GetEncoding(rsp.CharacterSet);
            return GetResponseAsString(rsp, encoding);
        }
    }
}