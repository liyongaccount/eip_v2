﻿/**************************************************************
* Copyright (C) 2018 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2018/10/30 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Common.Core.Auth;
using EIP.Common.Core.Context;
using EIP.Common.Language.Resource;
using EIP.Common.Models;
using EIP.Common.Models.Paging;
using EIP.Common.Models.Tree;
using EIP.Common.Util;
using Magicodes.ExporterAndImporter.Core;
using Magicodes.ExporterAndImporter.Excel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace EIP.Common.Controller
{
    /// <summary>
    /// 继承BaseController都必须要继续授权验证
    /// </summary>
    [Route("api/[controller]/[action]")]
    [Authorize("permission")]
    [ApiController]
    public class BaseController : Microsoft.AspNetCore.Mvc.Controller
    {
        protected readonly PrincipalUser CurrentUser;
        /// <summary>
        /// 用户构造函数
        /// </summary>
        public BaseController()
        {
            CurrentUser = EipHttpContext.CurrentUser();
        }
        #region Json

        /// <summary>
        /// 读取树结构:排除下级
        /// </summary>
        /// <param name="treeEntitys">TreeEntity的集合</param>
        /// <param name="opened"></param>
        /// <returns></returns>
        protected JsonResult JsonForJstree(IList<JsTreeEntity> treeEntitys, bool opened = false)
        {
            if (treeEntitys == null || treeEntitys.Count <= 0)
            {
                treeEntitys = new List<JsTreeEntity>();
                return Json(treeEntitys);
            }
            IList<JsTreeEntity> returnTreeEntities = new List<JsTreeEntity>(treeEntitys.Count);
            foreach (var item in treeEntitys)
            {
                //判断有无父级
                var ds = treeEntitys.Where(w => w.id.ToString() == item.parent.ToString());
                if (!ds.Any())
                {
                    item.parent = "#";
                    item.state = new JsTreeStateEntity { opened = opened };
                    returnTreeEntities.Add(item);
                }
                else
                {
                    if (ds.Any())
                    {
                        item.parent = Guid.Parse(item.parent.ToString()) == Guid.Empty ? "#" : item.parent;
                        item.state = new JsTreeStateEntity { opened = opened };
                        returnTreeEntities.Add(item);
                    }
                    else
                    {
                        item.parent = "#";
                        item.state = new JsTreeStateEntity { opened = opened };
                        returnTreeEntities.Add(item);
                    }
                }
            }
            return Json(returnTreeEntities);
        }
        /// <summary>
        /// 读取树结构:排除下级
        /// </summary>
        /// <param name="treeEntitys">TreeEntity的集合</param>
        /// <param name="opened"></param>
        /// <returns></returns>
        protected IList<JsTreeEntity> JsonForJstreeList(IList<JsTreeEntity> treeEntitys, bool opened = false)
        {
            if (treeEntitys == null || treeEntitys.Count <= 0)
            {
                treeEntitys = new List<JsTreeEntity>();
                return treeEntitys;
            }
            IList<JsTreeEntity> returnTreeEntities = new List<JsTreeEntity>(treeEntitys.Count);

            foreach (var data in treeEntitys)
            {
                //判断有无父级
                var ds = treeEntitys.Where(w => w.id.ToString() == data.parent.ToString());
                if (!ds.Any())
                {
                    data.parent = "#";
                    data.state = new JsTreeStateEntity { opened = opened };
                    returnTreeEntities.Add(data);
                }
                else
                {
                    if (ds.Any())
                    {
                        data.parent = Guid.Parse(data.parent.ToString()) == Guid.Empty ? "#" : data.parent;
                        data.state = new JsTreeStateEntity { opened = opened };
                        returnTreeEntities.Add(data);
                    }
                    else
                    {
                        data.parent = "#";
                        data.state = new JsTreeStateEntity { opened = opened };
                        returnTreeEntities.Add(data);
                    }
                }
            }
            return returnTreeEntities;
        }

        /// <summary>
        /// 返回分页后信息
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="pagedResults"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        protected JsonResult JsonForGridPaging<T>(OperateStatus<PagedResults<T>> pagedResults)
        {
            return Json(new
            {
                code = pagedResults.Code,
                pagedResults.Msg,
                page = pagedResults.Data.PagerInfo?.Current ?? 0,
                count = pagedResults.Data.PagerInfo?.RecordCount ?? 0,
                data = pagedResults.Data.Data
            });
        }

        /// <summary>
        /// 返回一次性数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="datas"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        protected JsonResult JsonForGridLoadOnce<T>(IEnumerable<T> datas, string msg = "")
        {
            var enumerable = datas as IList<T> ?? datas.ToList();
            return Json(new
            {
                code = 200,
                msg,
                count = enumerable.ToList().Count,
                data = enumerable
            });
        }

        /// <summary>
        ///  返回数据转换为JsTree
        /// </summary>
        /// <param name="datas"></param>
        /// <param name="opened"></param>
        /// <returns></returns>
        protected JsonResult JsonForJsTree(IEnumerable<JsTreeEntity> datas, bool opened = false)
        {
            IList<JsTreeEntity> output = new List<JsTreeEntity>();
            datas = datas.ToList();
            foreach (var data in datas)
            {
                //判断有无父级
                var ds = datas.Where(w => w.id.ToString() == data.parent.ToString()).ToList();
                if (!ds.Any())
                {
                    data.parent = "#";
                    data.state = new JsTreeStateEntity { opened = opened };
                    output.Add(data);
                }
                else
                {
                    data.state = new JsTreeStateEntity { opened = opened };
                    output.Add(data);
                }
            }
            return Json(output);
        }

        /// <summary>
        ///  返回数据转换为JsTree
        /// </summary>
        /// <param name="datas"></param>
        /// <returns></returns>
        protected JsonResult JsonForTree(IEnumerable<BaseTree> datas)
        {
            List<TreeEntity> output = new List<TreeEntity>();
            datas = datas.ToList();
            foreach (var data in datas)
            {
                if (data.parent == null)
                {
                    data.parent = Guid.Empty.ToString();
                }
                else
                {
                    //判断有无父级
                    var ds = datas.Where(w => w.id.ToString() == data.parent.ToString()).ToList();
                    if (!ds.Any())
                    {
                        data.parent = Guid.Empty.ToString();
                    }
                }
            }

            foreach (var data in datas.Where(w => w.parent.ToString() == Guid.Empty.ToString()))
            {
                TreeEntity treeEntity = new TreeEntity
                {
                    Value = data.id,
                    DisableCheckbox = data.disableCheckbox,
                    Key = data.id,
                    Title = data.text,
                    Disabled = data.disabled,
                    Slots = new TreeEntitySlots
                    {
                        Icon = string.IsNullOrEmpty(data.icon) ? "hdd" : data.icon,
                        Theme = string.IsNullOrEmpty(data.theme) ? "outlined" : data.theme,
                        Extend = data.extend,
                    }
                };
                treeEntity.Children = FindJsonForTreeChildren(datas.ToList(), data);
                if (!treeEntity.Children.Any())
                {
                    treeEntity.IsLeaf = true;
                }
                output.Add(treeEntity);
            }
            OperateStatus<List<TreeEntity>> operateStatus = new OperateStatus<List<TreeEntity>>();
            operateStatus.Code = ResultCode.Success;
            operateStatus.Msg = Chs.Successful;
            operateStatus.Data = output;
            return Json(operateStatus);
        }

        /// <summary>
        /// 递归获取树形结构
        /// </summary>
        /// <param name="datas"></param>
        /// <param name="treeEntity"></param>
        private List<TreeEntity> FindJsonForTreeChildren(List<BaseTree> datas, BaseTree treeEntity)
        {
            List<TreeEntity> treeEntities = new List<TreeEntity>();
            var children = datas.Where(w => w.parent.ToString() == treeEntity.id.ToString());
            foreach (var item in children)
            {
                TreeEntity treeEntityChidren = new TreeEntity
                {
                    Value = item.id,
                    DisableCheckbox = item.disableCheckbox,
                    Key = item.id,
                    Title = item.text,
                    Slots = new TreeEntitySlots
                    {
                        Icon = string.IsNullOrEmpty(item.icon) ? "hdd" : item.icon,
                        Theme = string.IsNullOrEmpty(item.theme) ? "outlined" : item.theme,
                        Extend = item.extend,
                    }
                };
                treeEntityChidren.Children = FindJsonForTreeChildren(datas, item);
                if (!treeEntityChidren.Children.Any())
                {
                    treeEntityChidren.IsLeaf = true;
                }
                treeEntities.Add(treeEntityChidren);
            }
            return treeEntities;
        }

        /// <summary>
        /// 检查值是否相同
        /// </summary>
        /// <param name="operateStatus"></param>
        /// <returns></returns>
        protected JsonResult JsonForCheckSameValue(OperateStatus operateStatus)
        {
            return Json(new
            {
                info = operateStatus.Msg,
                status = operateStatus.Code == ResultCode.Success ? "y" : "n"
            });
        }

        /// <summary>
        /// 检查值是否相同
        /// </summary>
        /// <param name="operateStatus"></param>
        /// <returns></returns>
        protected JsonResult JsonForCheckSameValueValidator(OperateStatus operateStatus)
        {
            return Json(new
            {
                valid = operateStatus.Code == ResultCode.Success
            });
        }

        #endregion

        #region 扩展
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            //if (!ModelState.IsValid)
            //{
            //    throw new EipException("参数验证错误");
            //}
            base.OnActionExecuting(context);
        }

        /// <summary>
        /// 检查Model参数
        /// </summary>
        /// <returns></returns>
        protected OperateStatus CheckModelState()
        {
            OperateStatus operateStatus = new OperateStatus();
            if (!ModelState.IsValid)
            {
                var msg = string.Empty;
                foreach (var value in ModelState.Values)
                {
                    if (value.Errors.Count > 0)
                    {
                        foreach (var error in value.Errors)
                        {
                            msg = msg + error.ErrorMessage + ",";
                        }
                    }
                }
                operateStatus.Msg = msg.TrimEnd(',');
            }
            else
            {
                operateStatus.Code = ResultCode.Success;
            }
            return operateStatus;
        }

        #endregion

        #region 导入导出
        /// <summary>
        /// 生成模版
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        protected async Task<FileResult> GenerateTemplate<T>() where T : class, new()
        {
            var path = ConfigurationUtil.GetUploadPath() + "/import/template";
            if (!Directory.Exists(path)) Directory.CreateDirectory(path);
            path = Path.Combine(path, nameof(T) + ".xlsx");
            if (System.IO.File.Exists(path)) System.IO.File.Delete(path);
            IImporter importer = new ExcelImporter();
            var result = await importer.GenerateTemplate<T>(path);
            return File(FileUtil.ReadFileStream(result.FileName), result.FileType);
        }

        /// <summary>
        /// 导入
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        protected async Task<OperateStatus<IList<T>>> Import<T>() where T : class, new()
        {
            var result = new OperateStatus<IList<T>>()
            {
                Data = new List<T>()
            };
            var files = Request.Form.Files;
            if (files.Any())
            {
                var file = files[0];
                var uploadPath = ConfigurationUtil.GetUploadPath() + "/import";
                if (!Directory.Exists(uploadPath)) Directory.CreateDirectory(uploadPath);
                string filepath = Path.Combine(uploadPath, file.FileName);
                if (System.IO.File.Exists(filepath)) System.IO.File.Delete(filepath);
                using (FileStream fs = System.IO.File.Create(filepath))
                {
                    file.CopyTo(fs);
                    fs.Flush();
                }
                IImporter importer = new ExcelImporter();
                var import = await importer.Import<T>(filepath);
                if (import.Exception != null)
                {
                    result.Msg = import.Exception.ToString();
                    return result;
                }
                if (import.RowErrors.Count > 0)
                {
                    result.Msg = JsonConvert.SerializeObject(import.RowErrors);
                    return result;
                }
                result.Data = import.Data.ToList();
            }
            result.Code = ResultCode.Success;
            result.Msg = Chs.Successful;
            return result;
        }
        /// <summary>
        /// 导出
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        protected async Task<FileResult> Export<T>(ICollection<T> datas) where T : class, new()
        {
            var path = ConfigurationUtil.GetUploadPath() + "/export";
            if (!Directory.Exists(path)) Directory.CreateDirectory(path);
            path = Path.Combine(path, nameof(T) + ".xlsx");
            IExporter exporter = new ExcelExporter();
            var result = await exporter.Export(path, datas);
            return File(FileUtil.ReadFileStream(result.FileName), result.FileType);
        }
        #endregion
    }
}